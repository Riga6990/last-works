import ObAddressInput from '@/obComponents/obAddressInput'
import { mount, createLocalVue } from '@vue/test-utils'
import Vue from 'Vue'

const EventBus = new Vue()

const GlobalPlugins = {
  install (v) {
    v.prototype.$eventGlobal = EventBus
  }
}
const localVue = createLocalVue()

localVue.use(GlobalPlugins)

describe('obAddressInput', () => {
  test('input value should be saved in currentAddress', async () => {
    const wrapper = mount(ObAddressInput, { localVue })
    wrapper.find('input').setValue('Москва')
    expect(wrapper.vm.currentAddress).toEqual('Москва')
  })
})
