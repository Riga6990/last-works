import ObLayout from './obLayout'
import ObSinglePane from './obSinglePanels'
import ObTwoPanes from './obTwoPanels'

export { ObLayout, ObSinglePane, ObTwoPanes }
