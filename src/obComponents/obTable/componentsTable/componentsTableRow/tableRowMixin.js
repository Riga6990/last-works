import getCellHtmlMixin from './getCellHtmlMixin'

export default {
  data () {
    return {
      value: null
    }
  },
  mixins: [
    getCellHtmlMixin
  ],
  props: {
    data: {
      type: Object,
      required: true
    },
    column: {
      type: Object,
      required: true
    },
    valueType: {
      type: String,
      required: true
    },
    isPending: {
      type: Boolean,
      default: false
    },
    dataQa: {
      type: String
    }
  },
  computed: {
    isDisabled () {
      return this.isPending || this.column.isDisabled
    },
    highlightClass ({ column }) {
      /**
       * There are 3 ways of how to use 'highlight' property for columns:
       * 1. Function. This function will be fired each time
       * when users type something into the field. If the
       * returned value of this function is 'string' type,
       * it will be used as a highlight status, otherwise,
       * function result will be checked as boolean (true/false);
       * 2. String. If value in the field is invalid, status
       * from 'highlight' property will be using highlight status;
       * 3. Boolean. If value in the field is invalid,
       * highlight color will be in 'error' status.
       */

      if (!column || column.isDisabled || !column.highlight) {
        return
      }

      let status = null

      if (typeof column.highlight === 'function') {
        const result = column.highlight(this.value)

        if (typeof result === 'string') {
          status = column.highlight(this.value)
        } else if (!result) {
          return
        }
      } else {
        const isValueValid = () => {
          switch (this.valueType) {
            case 'CHECKLIST':
            case 'CHECKLIST_LIST':
              return this.value !== null

            case 'DATE':
            case 'DATETIME':
              // Ref element can be non-ready at this moment
              if (!this.$refs.datepicker) {
                return true
              }

              return this.$refs.datepicker.localValue && this.$refs.datepicker.localValue === this.$refs.datepicker.date

            default:
              return this.value
          }
        }

        if (isValueValid()) {
          return
        }

        if (typeof column.highlight === 'string') {
          status = column.highlight
        }
      }

      switch (status) {
        case 'error':
          return 'isError'

        case 'warning':
          return 'isWarning'

        default:
          return 'isError'
      }
    }
  },
  methods: {
    async setValue (value) {
      // Using $nextTick to works handler() method
      await this.$nextTick()

      this.value = value

      // For fields with 'isEditHtml' we cannot set value - they are doesn't have keys
      if (!this.column.isEditHtml) {
        // Updating field value in table row
        this.data.item[this.column.key] = value
      }
    },
    getValue () {
      return this.column.isEditHtml ? this.value : this.data.item[this.column.key]
    },
    onFieldFocus () {
      this.setFocusState({
        isFocus: true
      })
    },
    onFieldBlur (sendRowData = true) {
      this.setFocusState({
        isFocus: false,
        lastValue: this.getValue(),
        sendRowData
      })
    },
    async onFieldChanged (value) {
      this.$emit('on-field-modified')

      // If need to make some changes with value,
      // for example - absolute negative numbers
      if (this.column.handler) {
        value = this.column.handler(value)
      }

      await this.setValue(value)
    },
    setFocusState ({ isFocus, lastValue = null, sendRowData = true }) {
      this.$emit('change-focus-state', ...arguments)
    }
  }
}
