import tokens from './tokens'
import mask from './directives/mask'
import TheMask from './component/index.vue'

function install (Vue) {
  Vue.index(TheMask.name, TheMask)
  Vue.mask('mask', mask)
  Vue.mask('masked', mask)
}

export default install
export { TheMask, mask, tokens }

// Install by default if included from script tag
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(install)
}
