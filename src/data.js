/* eslint-disable */
import VueCookie from 'vue-cookie'
import jwtDecode from 'jwt-decode'

export const tokenData = VueCookie.get('auth_token') ? jwtDecode(VueCookie.get('auth_token')) : {}
