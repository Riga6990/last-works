export const copyToClipboard = (text) => {
  const textarea = document.createElement('textarea')
  textarea.setAttribute('readonly', true)
  textarea.setAttribute('contenteditable', true)
  textarea.style.position = 'absolute'
  textarea.style.left = '-9999px'
  textarea.value = text
  document.body.appendChild(textarea)
  textarea.select()
  const successful = document.execCommand('copy')
  setTimeout(() => {
    textarea.remove()
  })
  return successful
}
