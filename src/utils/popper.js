let popperPositionCount = 0

export default class Popper {
  constructor (props) {
    const { reference, popper, options } = props

    this.state = {}
    this.reference = reference
    this.popper = popper
    this.options = options
    this.referenceBound = {}

    this.scrollContainer = this.reference.closest('.ob-modal-container')

    this.resizeObserver = new ResizeObserver(entries => {
      // for (let entry of entries) {
      // }
      this.update()
    })

    this.setupEventListeners()
  }
  update () {
    popperPositionCount = 0

    this.resetStyles()
    this.referenceBound = this.reference.getBoundingClientRect()
    let { top, right, bottom, left, width, height } = this.referenceBound
    let { position, inheritWidth, boundPadding, minWidth } = this.options
    boundPadding = Array.isArray(boundPadding) ? boundPadding : [ boundPadding || 0, boundPadding || 0 ]
    let clientWidth = document.body.clientWidth
    let clientHeight = document.body.clientHeight
    let scrollTop = window.scrollY
    let popperSizes = this.getOuterSizes(this.popper)
    let popperWidth = popperSizes.width
    let popperHeight = popperSizes.height
    let offsetTop
    let offsetLeft

    let secondPosition = position.split('-')[1] || null
    position = position.split('-')[0]
    let initPosition = position

    let topBottom = ['top', 'bottom']
    let leftRight = ['left', 'right']
    let startEnd = ['start', 'end']

    if (position === 'bottom') {
      topBottom = topBottom.reverse()
    }
    if (position === 'right') {
      leftRight = leftRight.reverse()
    }
    if (position === 'end') {
      startEnd = startEnd.reverse()
    }

    const isInTableCell = this.reference.closest('.td-container')
    const maxWidthInTableCell = 400

    if (minWidth && (popperWidth < minWidth)) {
      popperWidth = minWidth
      this.popper.style.width = popperWidth + 'px'
    }

    if (inheritWidth && !minWidth) {
      popperWidth = width
      this.popper.style.width = popperWidth + 'px'
    }

    if (isInTableCell && (popperWidth > maxWidthInTableCell)) {
      popperWidth = maxWidthInTableCell
      this.popper.style.width = popperWidth + 'px'
    }

    let positionMap = [ ...topBottom, ...leftRight ]
    let initPositionIndex = positionMap.indexOf(position)
    let correctPosition = false

    let secondPositionMap = [ ...startEnd ]
    let initSecondPositionIndex = secondPositionMap.indexOf(secondPosition)
    let correctSecondPosition = !secondPosition

    let calcPosition = (pos, secondPos, force = false) => {
      let positionIndex = positionMap.indexOf(pos)
      let secondPositionIndex = secondPositionMap.indexOf(secondPos)

      if (pos === 'top') {
        correctPosition = (top - boundPadding[1]) >= popperHeight
        offsetTop = scrollTop + top - popperHeight - boundPadding[1]
        offsetLeft = left + (width / 2) - (popperWidth / 2)

        if (secondPos === 'start') {
          correctSecondPosition = (clientWidth - left + boundPadding[0]) >= popperWidth
          offsetLeft = left - boundPadding[0]
        } else if (secondPos === 'end') {
          correctSecondPosition = right + boundPadding[0] >= popperWidth
          offsetLeft = left + width + boundPadding[0] - popperWidth
        }
      } else if (pos === 'bottom') {
        correctPosition = (clientHeight - (scrollY + bottom + boundPadding[1])) >= popperHeight
        offsetTop = scrollTop + bottom + boundPadding[1]
        offsetLeft = left + (width / 2) - (popperWidth / 2)
        if (secondPos === 'start') {
          correctSecondPosition = (clientWidth - left + boundPadding[0]) >= popperWidth
          offsetLeft = left - boundPadding[0]
        } else if (secondPos === 'end') {
          correctSecondPosition = right + boundPadding[0] >= popperWidth
          offsetLeft = left + width + boundPadding[0] - popperWidth
        }
      } else if (pos === 'left') {
        correctPosition = (left - boundPadding[0]) >= popperWidth
        offsetTop = scrollTop + top + (height / 2) - (popperHeight / 2)
        offsetLeft = left - popperWidth - boundPadding[0]

        if (secondPos === 'start') {
          correctSecondPosition = (clientHeight - top + boundPadding[1]) >= popperHeight
          offsetTop = scrollTop + top - boundPadding[1]
        } else if (secondPos === 'end') {
          correctSecondPosition = bottom + boundPadding[1] >= popperHeight
          offsetTop = scrollTop + top + (height - popperHeight + boundPadding[1])
        }
      } else if (pos === 'right') {
        correctPosition = (clientWidth - right - boundPadding[0]) >= popperWidth
        offsetTop = scrollTop + top + (height / 2 - popperHeight / 2)
        offsetLeft = right + boundPadding[0]

        if (secondPos === 'start') {
          correctSecondPosition = (clientHeight - top + boundPadding[1]) >= popperHeight
          offsetTop = scrollTop + top - boundPadding[1]
        } else if (secondPos === 'end') {
          correctSecondPosition = bottom + boundPadding[1] >= popperHeight
          offsetTop = scrollTop + top + (height - popperHeight + boundPadding[1])
        }
      }
      if ((correctPosition && correctSecondPosition) || force) {
        initPositionIndex = positionMap.indexOf(pos)
        initSecondPositionIndex = secondPositionMap.indexOf(secondPos)

        this.setArrowPosition({ position: pos, top: offsetTop, left: offsetLeft, width: popperWidth, height: popperHeight })

        return {
          top: offsetTop,
          left: offsetLeft
        }
      } else {
        const nextPosition = correctPosition ? pos : positionMap[positionIndex + 1] || positionMap[0]
        const isLastPosition = nextPosition === initPosition

        const nextSecondPosition = correctPosition ? secondPositionMap[secondPositionIndex + 1] || secondPositionMap[0] : secondPos
        const isLastSecondPosition = nextSecondPosition === secondPositionIndex

        let isForce = (isLastPosition && isLastSecondPosition) || popperPositionCount > 20

        popperPositionCount += 1

        return calcPosition(nextPosition, nextSecondPosition, isForce)
      }
    }
    this.setPosition(calcPosition(position, secondPosition), {
      position: positionMap[initPositionIndex],
      secondPosition: secondPositionMap[initSecondPositionIndex],
      width: popperWidth,
      height: popperHeight
    })
  }
  resetStyles () {
    this.popper.style.removeProperty('width')
  }
  destroy () {
    this.removeEventListeners()
    return this
  }
  setPosition ({ top, left } = {}, { position, width, height } = {}) {
    this.popper.style.top = top + 'px'
    this.popper.style.left = left + 'px'
    this.popper.classList.remove(`--top`)
    this.popper.classList.remove(`--bottom`)
    this.popper.classList.remove(`--right`)
    this.popper.classList.remove(`--left`)
    this.popper.classList.add(`--${position}`)
  }
  setArrowPosition ({ position, top, left, width, height }) {
    let arrow = this.popper.querySelector('.popper__arrow')
    if (!arrow) return
    let positionKey = ''
    let positionValue = 0
    if (['top', 'bottom'].includes(position)) {
      positionKey = 'left'
      positionValue = (this.referenceBound.left - left) + (this.referenceBound.width / 2)
    } else {
      positionKey = 'top'
      positionValue = (this.referenceBound.top - top) + (this.referenceBound.height / 2)
    }

    arrow.setAttribute('style', `${positionKey}: ${positionValue}px`)
  }
  getOuterSizes (element) {
    element.style.visibility = 'visible'
    let styles = window.getComputedStyle(element)
    let x = parseFloat(styles.marginTop) + parseFloat(styles.marginBottom)
    let y = parseFloat(styles.marginLeft) + parseFloat(styles.marginRight)

    return { width: element.offsetWidth + y, height: element.offsetHeight + x }
  }

  setupEventListeners () {
    // NOTE: 1 DOM access here
    this.state.updateBound = this.update.bind(this)

    window.addEventListener('resize', this.state.updateBound)
    if (this.scrollContainer) {
      this.scrollContainer.addEventListener('scroll', this.state.updateBound)
    }

    this.resizeObserver.observe(this.popper)
  }

  removeEventListeners () {
    // NOTE: 1 DOM access here
    window.removeEventListener('resize', this.state.updateBound)
    if (this.scrollContainer) {
      this.scrollContainer.removeEventListener('scroll', this.state.updateBound)
    }
    this.state.updateBound = null
  }
}
