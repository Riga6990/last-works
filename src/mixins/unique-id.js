import { uuid } from 'vue-uuid'

export default {
  data () {
    return {
      id: null,
      uuid: uuid.v4()
    }
  },
  mounted () {
    this.$eventGlobal.$on('setComponentId', this.setComponentId)

    this.$eventGlobal.$emit('getComponentId', {
      uuid: this.uuid,
      tag: this.$options.name.substr(2).toLowerCase()
    })
  },
  methods: {
    setComponentId (data) {
      if (data.uuid === this.uuid) {
        this.id = data.id
      }
    }
  }
}
