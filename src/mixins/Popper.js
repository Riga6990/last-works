import Vue from 'vue'
import Popper from '../utils/popper'

export default {
  props: {
    target: {
      type: String,
      default: null
    },
    disabled: {
      type: Boolean,
      default: false
    },
    popperClass: {
      type: String,
      default: null
    },
    position: {
      type: String,
      default: 'top'
    },
    type: {
      type: String,
      default: 'primary'
    },
    content: {
      type: [String, Object],
      default: null
    },
    appendToBody: {
      type: Boolean,
      default: true
    },
    openDelay: {
      type: Number,
      default: 500
    },
    closeAfter: {
      type: Number,
      default: 0
    },
    value: {
      type: Boolean,
      default: false
    },
    popperOpt: {
      type: Object,
      default: () => {}
    }
  },
  beforeCreate () {
    this.popperVM = new Vue({
      data: { node: '' },
      render (h) {
        return this.node
      }
    }).$mount()
  },
  beforeDestroy () {
    this.popperVM && this.popperVM.$destroy()
    this.doDestroy(true)
    if (this.popperElm && this.popperElm.parentNode === document.body) {
      this.popperElm.removeEventListener('click', stop)
      document.body.removeChild(this.popperElm)
    }
  },
  data () {
    return {
      popperJS: null,
      focusing: false,
      timeoutOpen: null,
      timeoutClose: null,
      visible: false
    }
  },
  computed: {
    computedPopperOpt () {
      return {
        ...this.localPopperOpt,
        ...this.popperOpt
      }
    }
  },
  watch: {
    value: {
      immediate: true,
      handler (val) {
        this.changeVisible(val)
      }
    },
    visible (val) {
      if (val) {
        this.$nextTick(() => {
          this.updatePopper()
        })
      } else {
        this.doDestroy()
      }
    },
    disabled (val) {
      if (val) {
        this.changeVisible(false)
      }
    },
    focusing (val) {
      if (val) {
        this.referenceElm && this.referenceElm.classList.add('focusing')
      } else {
        this.referenceElm && this.referenceElm.classList.remove('focusing')
      }
    }
  },
  methods: {
    currentContent () {
      return this.$slots.content || this.content
    },
    changeVisible (state) {
      if (state && !this.disabled) {
        clearTimeout(this.timeoutClose)
        clearTimeout(this.timeoutOpen)
        this.timeoutOpen = setTimeout(() => {
          this.visible = true
          this.$emit('input', true)
          if (this.closeAfter) {
            this.timeoutClose = setTimeout(() => {
              this.visible = false
              this.$emit('input', false)
            }, this.closeAfter)
          }
        }, this.openDelay)
      } else if (!state) {
        clearTimeout(this.timeoutOpen)
        clearTimeout(this.timeoutClose)
        this.timeoutClose = setTimeout(() => {
          this.visible = false
          this.$emit('input', false)
        }, 0)
      }
    },
    handleFocus () {
      this.focusing = true
      this.changeVisible(true)
    },
    handleBlur () {
      this.focusing = false
      this.changeVisible(false)
    },
    removeFocusing () {
      this.focusing = false
    },
    getFirstElement () {
      const slots = this.$slots.default
      if (!Array.isArray(slots)) return null
      let element = null
      for (let index = 0; index < slots.length; index++) {
        if (slots[index] && slots[index].tag) {
          element = slots[index]
        }
      }
      return element
    },
    createPopper () {
      this.currentPlacement = this.position

      const popper = this.popperElm = this.popperElm || this.Popper || this.$refs.popper
      let reference = this.referenceElm = this.referenceElm || this.reference || this.$refs.reference
      let options = {
        position: this.position,
        hideArrow: this.hideArrow,
        ...this.computedPopperOpt
      }

      if (!reference &&
              this.$slots.reference &&
              this.$slots.reference[0]) {
        reference = this.referenceElm = this.$slots.reference[0].elm
      }

      if (!popper || !reference) return
      if (!this.hideArrow) this.appendArrow(popper)
      if (this.appendToBody) document.body.appendChild(this.popperElm)
      this.popperJS = new Popper({ reference, popper, options })
      this.$nextTick(() => {
        this.popperJS.update()
      })
    },

    updatePopper () {
      if (this.visible) {
        const popperJS = this.popperJS
        if (popperJS) {
          popperJS.update()
        } else {
          this.createPopper()
        }
      } else {
        this.doDestroy()
      }
    },

    doDestroy (forceDestroy) {
      if (!this.popperJS || (this.visible && !forceDestroy)) return
      this.popperJS.destroy()
      this.popperJS = null
    },

    appendArrow (popper) {
      if (popper.querySelector('.popper__arrow')) return
      let arrow = document.createElement('div')
      arrow.setAttribute('class', 'popper__arrow')
      popper.appendChild(arrow)
    }
  }
}
