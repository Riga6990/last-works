/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, select, object } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObDatepicker from '../obComponents/obRangeDatepicker/index'
import ObRadio from '../obComponents/obRadio/index.vue'

const defaultSinceTo = [
  {
    from: '2019-12-24',
    to: '2019-12-28 23:59'
  },
  {
    from: '2019-12-15',
    to: '2019-12-20 23:59'
  }
]

storiesOf('ObRangeDatepicker', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObDatepicker, ObRadio },
      props: {
        format: {
          type: String,
          default: text('format', 'dd mm yyyy')
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Date')
        },
        label: {
          type: String,
          default: text('label', 'Date')
        },
        sinceTo: {
          type: Array,
          default: object('sinceTo', defaultSinceTo)
        },
        blockAll: {
          type: Boolean,
          default: boolean('blockAll', false)
        },
        allowHolidays: {
          type: Boolean,
          default: boolean('allowHolidays', true)
        },
        allowWeekends: {
          type: Boolean,
          default: boolean('allowWeekends', false)
        },
        allowAll: {
          type: Boolean,
          default: boolean('allowAll', false)
        },
        lang: {
          default: select('lang', ['ru', 'en', 'zh'], 'ru')
        },
        isInline: {
          default: boolean('isInline', false)
        },
        isTime: {
          default: boolean('isTime', true)
        }
      },
      data () {
        return {
          date: { from: new Date('01.02.2020'), to: new Date() },
          radioModel: '0'
        }
      },
      template: `
        <div class="element_container">
          <ObDatepicker
            :format="format"
            :placeholder="placeholder"
            :label="label"
            :blockAll="blockAll"
            :sinceTo="sinceTo"
            :allowHolidays="allowHolidays"
            :allowWeekends="allowWeekends"
            :allowAll="allowAll"
            :lang="lang"
            :is-inline="isInline"
            :is-time="isTime"
            v-model="date"
          > 
          <div style="display:flex; justify-content: space-around">
            <ob-radio
              label="Погрузка"
              valueInput="0"
              v-model="radioModel"
            />
            
            <ob-radio
              label="Отгрузка"
              valueInput="1"
              v-model="radioModel"
            />
          </div>
          </ObDatepicker>
        </div>`
    }
  },
  {
    info: {
      components: { ObDatepicker },
      summary: `
      \`\`\`html
       <ObDatepicker
        :format="format"
        :placeholder="placeholder"
        :label="label"
        :blockAll="blockAll"
        :sinceTo="sinceTo"
        :allowHolidays="allowHolidays"
        :allowWeekends="allowWeekends"
        :allowAll="allowAll"
        :lang="lang"
        :is-inline="isInline"
        :is-time="isTime"
        v-model="date"
      > 
      <div style="display:flex; justify-content: space-around">
        <ob-radio
          label="Погрузка"
          valueInput="0"
          v-model="radioModel"
        />
        
        <ob-radio
          label="Отгрузка"
          valueInput="1"
          v-model="radioModel"
        />
      </div>
      </ObDatepicker>
      \`\`\`
      `,
      source: false
    }
  })
