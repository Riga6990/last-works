import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObShowMore from '../obComponents/obShowMore'

const stories = storiesOf('ObShowMore', module)
stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    value: {
      default: boolean('value', false)
    },
    height: {
      default: number('height', 150)
    },
    hideArrow: {
      default: boolean('hideArrow', false)
    },
    showText: {
      default: text('showText', 'Показать все')
    },
    hideText: {
      default: text('hideText', 'Скрыть')
    },
    isCompact: {
      default: boolean('isCompact', false)
    }
  },
  components: {
    ObShowMore
  },
  template: `
    <div class="element_container">
      <ObShowMore
        :value="value"
        :height="height"
        :hide-arrow="hideArrow"
        :show-text="showText"
        :hide-text="hideText"
        :is-compact="isCompact"
      >
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Cras efficitur at enim at dapibus. Sed eu quam justo.
        Integer vel sem feugiat, suscipit arcu nec, congue turpis.
        Suspendisse commodo eget urna nec mollis.
        Nullam semper augue sed magna sodales, sed sodales orci semper.
        Praesent fermentum feugiat lacus, at cursus erat malesuada non.
        Mauris vel libero quis erat auctor ornare. Praesent ac sodales risus.
        Nulla elit erat, pretium ut viverra nec, pulvinar non elit.

        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Fusce tortor nunc, bibendum vitae tortor eu, finibus mollis erat.
        Maecenas auctor enim eget auctor pretium. In hac habitasse platea dictumst.
        Etiam vel orci nec magna sagittis efficitur eget at ex.
        Aenean euismod rutrum ante in sollicitudin.
        Donec eu lacinia odio, eu egestas lectus.
      </ObShowMore>
    </div>
  `
}),
{
  info: {
    components: { ObShowMore },
    summary: `
      \`\`\`html
        <ObShowMore
          :value="value"
          :height="height"
          :hide-arrow="hideArrow"
          :show-text="showText"
          :hide-text="hideText"
          :is-compact="isCompact"
        > 
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </ObShowMore>
      \`\`\`
      `,
    source: false
  }
})
