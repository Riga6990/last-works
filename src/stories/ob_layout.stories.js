/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withInfo } from 'storybook-addon-vue-info'
import ObMainTemplate from '../obComponents/obMainTemplate'
import SinglePane from '../obComponents/obSinglePanels/index.vue'

storiesOf('ObLayout', module)
  .addDecorator(withInfo)
  .add('sample', () => ({
    data () {
      return {
        menu: []
      }
    },
    computed: {
    },
    components: { ObMainTemplate, SinglePane },
    template: `
        <ObMainTemplate @change-locale="onChangeLocale" :top-level-members="menu" @clickToObject="clickToObject">
            <!--<div class="content-wrapper" :class="[showPreload ? 'preload-on' : '']">-->
            <SinglePane capt="Заголовок">
                <div class="row">
                    <div class="col-xs-6">
                        Меню не отображается т.к. его формирует бэк
                    </div>
                    <div class="col-xs-6">
                        Меню не отображается т.к. его формирует бэк
                    </div>
                </div>
            </SinglePane>
            <!--      </div>-->
        </ObMainTemplate>`,
    methods: {
      clickToObject () {},
      onChangeLocale () {}
    }
  }),
  {
    info: {
      components: { ObMainTemplate, SinglePane },
      summary: `
          \`\`\`html
            <ObMainTemplate 
                :top-level-members="menu" 
                @change-locale="onChangeLocale" 
                @clickToObject="clickToObject"
            >
                <SinglePane capt="Заголовок">
                    <div class="row">
                        <div class="col-xs-6">
                            Меню не отображается т.к. его формирует бэк
                        </div>
                        <div class="col-xs-6">
                            Меню не отображается т.к. его формирует бэк
                        </div>
                    </div>
                </SinglePane>
            </ObMainTemplate>
          \`\`\`
      `,
      source: false
    }
  })
