/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import { action } from '@storybook/addon-actions'
import ObButton from '../obComponents/obButton/index.vue'

const stories = storiesOf('ObButton', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    label: {
      default: text('label', 'Секреты')
    },
    tooltip: {
      default: text('tooltip', '')
    },
    icon: {
      default: text('icon', '')
    },
    type: {
      default: select('type', [ 'primary', 'outline', 'flat', 'primary-success' ], 'primary')
    },
    isFullWidth: {
      default: boolean('width 100%', false)
    },
    disabled: {
      default: boolean('disabled', false)
    },
    mini: {
      default: boolean('mini', false)
    },
    danger: {
      default: boolean('danger', false)
    },
    dark: {
      default: boolean('dark', false)
    }
  },
  data () {
    return {
      demoCounter: 0
    }
  },
  components: { ObButton },
  template: `
    <div class="element_container">
        <ObButton 
          :label="label"
          :disabled="disabled"
          :dark="dark"
          :danger="danger"
          :type="type"
          :mini="mini"
          :isFullWidth="isFullWidth"
          :tooltip="tooltip"
          :icon="icon"
          @click="demoCounter++"
        />
      <br>
      <br>
      Demo counter on callback: {{ demoCounter }}
    </div>`,
  methods: { action: action('clicked') }
}),
{
  info: {
    components: { ObButton },
    summary: `
        \`\`\`html
        <ObButton 
          :label="label"
          :disabled="disabled"
          :dark="dark"
          :danger="danger"
          :type="type"
          :mini="mini"
          :isFullWidth="isFullWidth"
          :tooltip="tooltip"
          :icon="icon"
          @click="demoCounter++"
        />
        \`\`\`
      `,
    source: false
  }
})
