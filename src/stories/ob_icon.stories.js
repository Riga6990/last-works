/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import ObTooltip from '../obComponents/obTooltip'
import ObInput from '../obComponents/obInput'
import { copyToClipboard } from '../utils/clipboard'

storiesOf('ObIcon', module)
  .add('sample', () => ({
    components: {
      ObTooltip,
      ObInput
    },
    data () {
      return {
        label: 'Секреты',
        disabled: false,
        dark: false,
        danger: false,
        mini: false,
        type: 'primary',
        searchQuery: '',

        demoCounter: 0,
        icons: [
          'additional-round-0',
          'additional-round-1',
          'additional-round-2',
          'additional-round-3',
          'advance-0',
          'advance-1',
          'advance-2',
          'advance-3',
          'advance-4',
          'aglomeration',
          'airplane-sm',
          'airplane',
          'alert-xs',
          'alert',
          'arrow',
          'attach',
          'auction',
          'barcode',
          'battery_level',
          'battery',
          'bell-on',
          'bell',
          'blind-auction',
          'booking-0-1',
          'booking-0-2',
          'booking-0-3',
          'calendar-sm',
          'calendar',
          'call-center',
          'cargo-truck',
          'carriage-sm',
          'carriage',
          'check_white',
          'checked-full',
          'checked-mini',
          'checked-simple',
          'checked',
          'chevron-down',
          'chevron-left',
          'chevron-right',
          'chevron-up',
          'city-sm',
          'city',
          'clock-alarm',
          'clock-xs',
          'clock',
          'clockwatch',
          'close-sm',
          'close-xs',
          'close',
          'cold',
          'columns',
          'comment-sm',
          'comment',
          'commercial_blue',
          'composite',
          'container-20',
          'container-40',
          'container-sm',
          'container',
          'copy-2',
          'copy',
          'delete-xs',
          'delete',
          'digital-document',
          'document',
          'double-checked-simple',
          'double-checked',
          'download',
          'edit',
          'electric-locomotive',
          'email',
          'event',
          'excel',
          'exit',
          'external-link',
          'fence',
          'filter',
          'first-bid',
          'flag-finish',
          'flatbed-van-b-g',
          'flatbed-van-m-g',
          'flatbed-van-s-g',
          'fold-map',
          'folder',
          'frame',
          'freezing',
          'gantry-crane-sm',
          'gantry-crane',
          'gondola-car-dlinnobaznaya',
          'gondola-car-fitingovaya',
          'gondola-car-gluhodonnyj',
          'gondola-car-lyukovoj',
          'gondola-car-universalnaya',
          'graph',
          'group-list',
          'group',
          'h2s',
          'hand-box-sm',
          'hand-box',
          'hand-settings',
          'hand',
          'handshake',
          'hazard',
          'home',
          'hot-2',
          'hot',
          'import-integration',
          'important-sm',
          'important',
          'info',
          'inspection',
          'isothermal-van-b-g',
          'isothermal-van-m-g',
          'isothermal-van-s-g',
          'key',
          'light',
          'link-left',
          'link-mutual',
          'link-right',
          'link',
          'lock',
          'logo-gruzi',
          'map',
          'menu-favourite-filled',
          'menu-favourite',
          'minus',
          'mobile_app_blue',
          'mobile-xs',
          'mobile',
          'modal-list',
          'more',
          'move',
          'navigation-arrow',
          'next',
          'onboard_blue',
          'order-excel',
          'order-upload',
          'pause',
          'pin-filled',
          'pin',
          'pinned',
          'play',
          'plus-sm',
          'plus',
          'police-sm',
          'police',
          'present-time',
          'previous',
          'primary-order-gray',
          'print',
          'profile-eye-hide',
          'profile-eye',
          'profile',
          'question-sm',
          'question',
          'radar-1',
          'radar-2',
          'radar-disabled',
          'radio-button',
          'radius',
          'railway-carriage-area',
          'railway-carriage-covered',
          'railway-carriage-dumpkar',
          'railway-carriage-refrigerator',
          'railway-carriage-termos',
          'railway-carriage-well',
          'rain',
          'ready-to-link',
          'recognize',
          'refresh',
          'refrigerated-van-b-g',
          'refrigerated-van-m-g',
          'refrigerated-van-s-g',
          'reloading',
          'restrict-sm',
          'restrict',
          'risk-0-grey',
          'risk-1-blue',
          'risk-2-green',
          'risk-3-yellow',
          'risk-4-orange',
          'risk-5-red',
          'risk-6-black',
          'risk',
          'role-administrator',
          'role-agent',
          'role-carrier',
          'role-client-sm',
          'role-client',
          'role-domain-owner',
          'role-system-moderator',
          'room-temperature',
          'route-loading',
          'route-pass-throught',
          'route-transfer',
          'route-unloading',
          'route',
          'samolet',
          'save',
          'search',
          'settings',
          'share',
          'ship-sm',
          'ship',
          'solid-chevron-down',
          'sort-single',
          'sort',
          'speed',
          'state_blue',
          'sudno',
          'target',
          'tasks-active',
          'tasks-ready',
          'tiagach-b',
          'tiagach-l',
          'time',
          'tractor-semi-trailer-bortovoj-b',
          'tractor-semi-trailer-izoterm-b',
          'tractor-semi-trailer-refrizhirator-b',
          'tractor-semi-trailer-refrizhirator-tent-b',
          'tractor-semi-trailer-shassi-1-b',
          'train-sm',
          'train',
          'tral-and-bulldozer',
          'trash-sm',
          'trash',
          'truck-crane',
          'truck-icon',
          'truck-sm',
          'truck',
          'tune',
          'unpinned',
          'visitcard',
          'wagon-awning-b-g',
          'wagon-awning-m-g',
          'wagon-awning-s-g',
          'warehouse-area',
          'warehouse-cold',
          'warehouse-dry',
          'warehouse-freezer',
          'warehouse-refrigerator',
          'world-not',
          'world'
        ],
        iconsPath: ['razgr', 'zagr']
      }
    },
    computed: {
      i_wrap () {
        return {
          display: 'inline-block',
          padding: '10px'
        }
      },
      iconStyle () {
        return {
          cursor: 'pointer'
        }
      },
      resultItems () {
        if (this.searchQuery.length === 0) return this.icons
        return this.icons.filter(i => i.includes(this.searchQuery))
      }
    },
    template: `
      <div style="padding: 5px">
      <p>Если размер иконки меньше <strong>16px</strong>, то надо брать ту, где обводка <strong>толще</strong>, если такая имеется.</p>
      <p><strong>Гайдлайн от дизайнера:</strong><br />
        12px - xs<br />
        16px - sm<br />
        24px - общие
      </p>
      <br />
      <span class="line-color">
        <span style="font-size: 22px"><i class="icon role-client"></i></span>
        <span style="font-size: 12px"><i class="icon alert-xs -gray-window"></i></span>
        <span style="font-size: 14px"><i class="icon alert- -gray-window"></i></span>
        <span style="font-size: 16px"><i class="icon alert -deep-blue"></i></span>
        <span style="font-size: 18px"><i class="icon alert -cobalt"></i></span>
        <span style="font-size: 20px"><i class="icon alert -caribbean"></i></span>
        <span style="font-size: 22px"><i class="icon alert -light-raspberry-red"></i></span>
        <span style="font-size: 24px"><i class="icon alert -diamond-orange-yellow"></i></span>
        <span style="font-size: 26px"><i class="icon alert -purple"></i></span>
      </span>
      <span class="line-color">
        <span style="font-size: 12px"><i class="icon calendar-sm -cobalt-black"></i></span>
        <span style="font-size: 14px"><i class="icon calendar-sm -gray-window"></i></span>
        <span style="font-size: 16px"><i class="icon calendar-sm -deep-blue"></i></span>
        <span style="font-size: 18px"><i class="icon calendar -cobalt"></i></span>
        <span style="font-size: 20px"><i class="icon calendar -caribbean"></i></span>
        <span style="font-size: 22px"><i class="icon calendar -light-raspberry-red"></i></span>
        <span style="font-size: 24px"><i class="icon calendar -diamond-orange-yellow"></i></span>
        <span style="font-size: 26px"><i class="icon calendar -purple"></i></span>
      </span>
      <span class="line-color">
        <span style="font-size: 12px"><i class="icon close-xs -cobalt-black"></i></span>
        <span style="font-size: 14px"><i class="icon close-sm -gray-window"></i></span>
        <span style="font-size: 16px"><i class="icon close-sm -deep-blue"></i></span>
        <span style="font-size: 18px"><i class="icon close -cobalt"></i></span>
        <span style="font-size: 20px"><i class="icon close -caribbean"></i></span>
        <span style="font-size: 22px"><i class="icon close -light-raspberry-red"></i></span>
        <span style="font-size: 24px"><i class="icon close -diamond-orange-yellow"></i></span>
        <span style="font-size: 26px"><i class="icon close -purple"></i></span>
      </span>

      <div style="font-size: 26px; margin: 30px 0;">
        <hr /><br />
        <h5>with class="path1"</h5>

        <div :style="i_wrap" v-for="icon in iconsPath">
          <ob-tooltip :content="icon" position="top-start">
            <i class="icon" :class="icon" :style="iconStyle" @click="onCopyIcon(icon)"><span class="path1"></span></i>
          </ob-tooltip>
        </div>

        <br />
        <ob-input
          v-model='searchQuery'
          placeholder='Ведите название иконки'
          icon='search'
        />

        <div :style="i_wrap" v-for="icon in resultItems">
          <ob-tooltip :content="icon" position="top-start">
            <i class="icon" :class="icon" :style="iconStyle" @click="onCopyIcon(icon)" />
          </ob-tooltip>
        </div>
      </div>
      </div>`,
    methods: {
      action: action('clicked'),
      onCopyIcon (icon) {
        copyToClipboard(icon)
      }
    }
  }))
