/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ObUploader from '../obComponents/obUploader/index.vue'
import { withKnobs, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import api from '../api'

storiesOf('ObUploader', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    props: {
      multiple: {
        default: boolean('multiple', true)
      },
      isReadOnly: {
        default: boolean('isReadOnly', false)
      }
    },
    data () {
      return {
        files: [],
        uploadMethod: api.files.postFile
      }
    },
    components: { ObUploader },
    template: `
      <div class="element_container">

        <ObUploader
          :multiple="multiple"
          :is-read-only="isReadOnly"
          :upload-method="uploadMethod"
          @change="onChange"
        />

        <br><br>

        {{ files }}

      </div>`,
    methods: {
      onChange (files) {
        this.files = files
      }
    }
  }),
  {
    info: {
      components: { ObUploader },
      summary: `
      \`\`\`html
        <ObUploader
          :multiple="multiple"
          :is-read-only="isReadOnly"
          :upload-method="uploadMethod"
          @change="onChange"
        />
      \`\`\`
      `,
      source: false
    }
  })
