/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, select, object } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObDatepicker from '../obComponents/obOrderDatePicker/index'

const defaultSinceTo = [
  {
    from: '2019-11-18',
    to: '2019-11-21'
  }
]
storiesOf('ObOrderDatePicker', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObDatepicker },
      props: {
        format: {
          default: text('format', 'dd MMMM yyyy')
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Date')
        },
        label: {
          default: text('label', 'Дата и время')
        },
        sinceTo: {
          default: object('sinceTo', defaultSinceTo)
        },
        blockAll: {
          default: boolean('blockAll', false)
        },
        allowHolidays: {
          default: boolean('allowHolidays', false)
        },
        allowWeekends: {
          default: boolean('allowWeekends', false)
        },
        allowAll: {
          default: boolean('allowAll', false)
        },
        isOrder: {
          default: boolean('isOrder', true)
        },
        lang: {
          default: select('lang', ['ru', 'en', 'zh'], 'ru')
        }
      },
      data () {
        return {
          date: {
            date: new Date(),
            type: '0'
          }
        }
      },
      template: `
    <div class="element_container">
        <ObDatepicker
          :format="format"
          :placeholder="placeholder"
          :label="label"
          :blockAll="blockAll"
          :sinceTo="sinceTo"
          :allowHolidays="allowHolidays"
          :allowWeekends="allowWeekends"
          :allowAll="allowAll"
          :lang="lang"
          :isOrder="isOrder"
          v-model="date"
        />
    </div>`
    }
  },
  {
    info: {
      components: { ObDatepicker },
      summary: `
      \`\`\`html
        <ObDatepicker
          :format="format"
          :placeholder="placeholder"
          :label="label"
          :blockAll="blockAll"
          :sinceTo="sinceTo"
          :allowHolidays="allowHolidays"
          :allowWeekends="allowWeekends"
          :allowAll="allowAll"
          :lang="lang"
          :isOrder="isOrder"
          v-model="date"
        />
      \`\`\`
      `,
      source: false
    }
  })
