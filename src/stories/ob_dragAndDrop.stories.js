/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, object } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

import ObDrag from '../obComponents/obDrag'
import ObDrop from '../obComponents/obDrop'

storiesOf('ObDragAndDrop', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObDrag, ObDrop },
      props: {
        transferData: {
          type: Object,
          default: object('Transfer data', { currentIndex: 0, text: 'Maybe you need me after drop' })
        },

        icon: {
          type: String,
          default: text('Icon', 'move')
        },

        currentIndex: {
          type: [Number, String],
          default: text('current element index', '0')
        }
      },

      data () {
        return {
          items: [
            [
              { id: 0, title: 'item 1-1' },
              { id: 1, title: 'item 1-2' },
              { id: 2, title: 'item 1-3' },
              { id: 3, title: 'item 1-4' },
              { id: 4, title: 'item 1-5' },
              { id: 5, title: 'item 1-6' }
            ],
            [
              { id: 20, title: 'item 2-1' },
              { id: 21, title: 'item 2-2' },
              { id: 22, title: 'item 2-3' },
              { id: 23, title: 'item 2-4' },
              { id: 24, title: 'item 2-5' },
              { id: 25, title: 'item 2-6' }
            ]
          ]
        }
      },

      methods: {
        onDrop (target, index, $event) {
          const { fromListIndex, fromItemIndex } = $event
          const from = this.items[fromListIndex]
          const itemToMove = from.splice(fromItemIndex, 1)[0]
          target.splice(index, 0, itemToMove)
        }
      },

      template: `
        <div style='margin: 20px;'>
          <div style='margin: 20px;'>
            <h2 style='margin-bottom: 20px;'>Drag and drop</h2>
            <div style="display: flex">
              <div style="width: 300px; margin-right: 20px;">
                <div
                  v-for="(item, index) in items[0]"
                  :key="item.title"
                >
                  <ObDrop
                    @drop="onDrop(items[0], index, $event)"
                  >
                    <ObDrag
                      :transferData="{
                        fromListIndex: 0,
                        fromItemIndex: index,
                      }"
                    >
                      <div class="item" style='padding: 10px; margin: 10px 0; font-size: 1.2em; background: white;'>
                        {{ item.title }}
                      </div>
                    </ObDrag>
                  </ObDrop>
                </div>
              </div>
              <div style="width: 300px;">
                <div
                  v-for="(item, index) in items[1]"
                  :key="item.title"
                >
                  <ObDrop
                    @drop="onDrop(items[1], index, $event)"
                  >
                    <ObDrag
                      :transferData="{
                        fromListIndex: 1,
                        fromItemIndex: index
                      }"
                    >
                      <div class="item" style='padding: 10px; margin: 10px 0; font-size: 1.2em; background: white;'>
                        {{ item.title }}
                      </div>
                    </ObDrag>
                  </ObDrop>
                </div>
              </div>
            </div>
          </div>
        </div>`,

      description: {
        obDrag: {
          props: {
            transferData: 'Data that need to handle on drop',
            icon: 'Icon that used as a marker to show that this block can be dragged'
          }
        },
        obDrop: {
          events: {
            enter: 'Fires when you drag over drop zone',
            leave: 'Fires when you leave the drop zone',
            drop: 'Fires when you drop block on drop zone'
          }
        }
      }
    }
  },
  {
    info: {
      summary: `
        See \`actions\` tab to inspect drop event. Usage:
        \`\`\`html
          <ObDrag :transferData="{...some data you need}">
            some content...
          </ObDrag>
          <ObDrop @drop="someMethod">
            some content...
          </ObDrop>
        \`\`\`
      `,
      source: false
    }
  }
  )
