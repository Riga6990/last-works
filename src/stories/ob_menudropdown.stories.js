/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs'
import ObMenuDropdown from '../obComponents/obMenuDropdown'
import { withInfo } from 'storybook-addon-vue-info'

const stories = storiesOf('ObMenuDropdown', module)
stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    trigger: {
      default: select('trigger', ['click', 'hover'], 'click')
    },
    position: {
      default: select('position', ['end', 'start'], 'start')
    },
    label: {
      default: text('label', '')
    },
    icon: {
      default: text('icon', 'more')
    },
    isArrow: {
      default: boolean('isArrow', true)
    },
    isNoneButtonStyle: {
      default: boolean('isNoneButtonStyle', false)
    }
  },
  data () {
    return {
      items: [
        {
          title: 'Вариант действия 1',
          className: 'color-error',
          href: 'https://alfilatov.com/posts/run-chrome-without-cors/',
          icon: 'plus'
        },
        {
          title: 'Вариант действия 2',
          description: 'Плюс дескриптор к этому пункту, который объясняет, что зачем',
          className: 'color-success',
          to: { name: 'data-grid' },
          icon: 'plus'
        },
        {
          title: 'Вариант действия 3 длинный-предлинный, который не умещается',
          description: 'Плюс дескриптор к этому пункту, который объясняет, что зачем',
          method: this.onClickEdit,
          icon: 'plus'
        }
      ],
      items2: [
        {
          title: 'Вариант действия 1',
          className: 'color-error',
          href: 'https://alfilatov.com/posts/run-chrome-without-cors/'
        },
        {
          title: 'Вариант действия 2',
          description: 'Плюс дескриптор к этому пункту, который объясняет, что зачем',
          className: 'color-success',
          to: { name: 'data-grid' }
        },
        {
          title: 'Вариант действия 2',
          description: 'Плюс дескриптор к этому пункту, который объясняет, что зачем',
          className: 'color-success',
          to: { name: 'data-grid' },
          icon: 'plus'
        },
        {
          title: 'Вариант действия 3 длинный-предлинный, который не умещается',
          method: this.onClickEdit
        },
        {
          title: 'Вариант действия 2',
          description: 'Плюс дескриптор к этому пункту, который объясняет, что зачем',
          className: 'color-success',
          to: { name: 'data-grid' },
          icon: 'plus'
        }
      ],
      items3: [
        {
          title: 'Вариант действия 1',
          className: 'color-error',
          href: 'https://alfilatov.com/posts/run-chrome-without-cors/'
        },
        {
          title: 'Вариант действия 2',
          className: 'color-success',
          to: { name: 'data-grid' }
        },
        {
          title: 'Вариант действия 3 длинный-предлинный, который не умещается',
          method: this.onClickEdit
        }
      ]
    }
  },
  components: {
    ObMenuDropdown
  },
  template: `
      <div style='margin-top: 100px; margin-left: 400px; width: 350px; padding: 40px;'>
        <div :style="rowBlock">
          <ObMenuDropdown
            :items="items3"
            :trigger="trigger"
            :position="position"
            :label="label"
            :is-arrow="isArrow"
            :icon="icon"
            :isNoneButtonStyle="isNoneButtonStyle"
          />
        </div>
        <div :style="rowBlock">
          <ObMenuDropdown
            :items="items"
            :trigger="trigger"
            :position="position"
            :label="label"
            :is-arrow="isArrow"
            :icon="icon"
            :isNoneButtonStyle="isNoneButtonStyle"
          />
        </div>
        <div :style="rowBlock">
          <ObMenuDropdown
            :items="items2"
            :trigger="trigger"
            :position="position"
            :label="label"
            :is-arrow="isArrow"
            :icon="icon"
            :isNoneButtonStyle="isNoneButtonStyle"
          />
        </div>
      </div>`,
  methods: {
    action: action('clicked'),
    onClickEdit () {
      alert('onClickEdit')
    }
  },
  computed: {
    rowBlock () {
      return {
        backgroundColor: '#fff',
        marginBottom: '20px'
      }
    }
  }
}),
{
  info: {
    components: { ObMenuDropdown },
    summary: `
      \`\`\`html
        <ObMenuDropdown
          :items="items"
          :trigger="trigger"
          :position="position"
          :label="label"
          :is-arrow="isArrow"
          :icon="icon"
          :isNoneButtonStyle="isNoneButtonStyle"
        />
      \`\`\`
    `,
    source: false
  }
})
