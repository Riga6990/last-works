import { storiesOf } from '@storybook/vue'
import {
  withKnobs,
  text,
  object,
  array
} from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObOrderContractorCard from '../obComponents/obOrderContractorCard'

storiesOf('ObOrderContractorCard', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    components: { ObOrderContractorCard },
    props: {
      caption: {
        type: String,
        default: text('Caption', 'Карточка исполнителя рейса')
      },
      inData1: {
        type: Object,
        default: object('inData1', {
          vehicle: {
            name: 'Полуприцеп. Изотерм. 20 т / 86 м³ / 33 пал',
            icon: { name: 'tractor-semi-trailer-izoterm-b', color: 'green' }
          },
          status: {
            text: 'Поиск исполнителя с лучей ценой',
            counter: '30:00',
            icon: { name: 'clock', color: '#9DA0AC' }
          },
          entities: []
        })
      },
      inData2: {
        default: object('inData2', {
          vehicle: {
            name: 'Полуприцеп. Изотерм. 20 т / 86 м³ / 33 пал',
            icon: { name: 'tractor-semi-trailer-izoterm-b', color: 'green' }
          },
          status: {
            text: 'Заявка подписана Ожидание начала рейса',
            counter: '1:21:33',
            icon: { name: 'checked', color: '#10CF96' }
          },
          entities: [
            {
              type: 1,
              title: 'Тягач',
              risk: { icon: { name: 'risk-5-red' } },
              grzNum: 'К 324 ББ',
              grzReg: '199',
              info: 'Камаз / +7 (823) 589-34-15 / 2015 г. / В собственности'
            },
            {
              type: 1,
              title: 'Фура',
              risk: { icon: { name: 'risk-1-green' } },
              grzNum: 'АВ 3214',
              grzReg: '199',
              info: 'Тент / 20т / 86м³/ 33 пал / Гидроборт, задняя загрузка  / 2009 г. / В собственности'
            },
            {
              type: 2,
              risk: { icon: { name: 'risk-2-blue' } },
              title: 'Водитель',
              fio: 'Алексеев Вадим А.',
              info: '+7 (823) 589-34-15'
            }
          ]
        })
      },
      dropdownTabs: {
        default: array('dropdownTabs', [{ title: '--' }])
      }
    },
    template: `
      <div>

      <ObOrderContractorCard
        :caption="caption"
        :inData="inData1"
        :dropdownTabs="dropdownTabs"
      />
      
      <br/>
      
      <ObOrderContractorCard
        :caption="caption"
        :inData="inData2"
        :dropdownTabs="dropdownTabs"
      />
            
      </div>`
  }),
  {
    info: {
      components: { ObOrderContractorCard },
      summary: `
      \`\`\`html
        <ObOrderContractorCard
          :caption="caption"
          :inData="inData1"
          :dropdownTabs="dropdownTabs"
        />
      \`\`\`
      `,
      source: false
    }
  })
