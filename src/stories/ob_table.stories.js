/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { boolean, text, withKnobs, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

import ObTable from '../obComponents/obTable'
import ObFuncPanel from '../obComponents/obFuncPanel'
import ObCheckbox from '../obComponents/obCheckbox'

storiesOf('ObTable', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    data () {
      return {
        filterButtons: [
          { html: '<span style="font-size:18px; color:red" class="icon inherit clock "></span>' },
          { title: 'Все' },
          { title: 'Актуальные' },
          { title: 'Неактивные' },
          { html: '<span style="color:red" class="icon email"></span> Почта' }
        ],
        addButtons: [
          { id: 'RFI',
            text: 'RFI (Request for information)'
          },
          { id: 'RFQ',
            text: 'RFQ (Request for quotation)'
          }
        ],
        addButtonsOptions: {
          group: true,
          text: 'Создать тендер'
        },
        items: [
          {
            title: 'first title',
            value: 'this is a value',
            when: '2019-10-10T10:49:53.747195Z',
            enabled: false,
            status: {
              enabled: false
            }
          },
          {
            title: 'second title',
            value: false,
            when: '2019-10-13T13:49:53.747195Z',
            enabled: false,
            status: {
              enabled: false
            }
          },
          {
            title: 'third title',
            value: 'this is some data',
            when: '2019-10-12T12:49:53.747195Z',
            enabled: true,
            status: {
              enabled: true
            }
          },
          {
            title: 'fourth title',
            value: 'just surfin',
            when: '2019-10-11T11:49:53.747195Z',
            enabled: false,
            status: {
              enabled: false
            }
          },
          {
            title: 'fifth tutle',
            value: 'this is test',
            when: '2019-10-09T11:49:53.747195Z',
            enabled: false,
            status: {
              enabled: false
            }
          }
        ],
        columns: [
          {
            title: 'Название',
            headerHtml: (data) => `<i class="icon alert"></i><span>${data.title}</span>`,
            key: 'title'
          },
          {
            title: 'Довольно таки длинная шапка столбца',
            titleInfo: 'Эта шапка такая длинная, потому что надо было ввести побольше символов',
            key: 'value',
            width: 200,
            sortDisable: true,
            action: data => {}
          },
          {
            slotName: 'enabled',
            title: 'Активен',
            key: 'enabled',
            width: 100
          },
          {
            title: 'Дата и время',
            key: 'when',
            html: data => this.getDatetime(data),
            sort: 'datetime'
          }
        ],
        columns2: [
          {
            title: 'Название',
            key: 'title',
            _isEnabled: true,
            uuid: 1
          },
          {
            title: 'Значение',
            key: 'value',
            _isEnabled: false,
            action: data => {},
            uuid: 2
          },
          {
            title: 'Дата и время',
            key: 'when',
            html: data => this.getDatetime(data),
            _isEnabled: true,
            sort: 'datetime',
            uuid: 3
          }
        ],
        columns3: [
          {
            title: 'Простой текст',
            key: 'text',
            withFilter: true,
            isEditColumn: true,
            highlight: 'warning',
            isWarning: true
          },
          {
            title: 'Деньги',
            key: 'money',
            withFilter: true,
            isEditColumn: true,
            highlight: true,
            isAlignRight: true,
            valueType: 'MONEY',
            handler: data => Math.abs(data)
          },
          {
            title: 'Список',
            key: 'checklist',
            withFilter: true,
            isEditColumn: true,
            highlight: true,
            // isClearable: true, // Column may be 'highlight' or 'isClearable', not both
            items: [
              {
                uuid: 1,
                title: 'first'
              },
              {
                uuid: 2,
                title: 'second'
              },
              {
                uuid: 3,
                title: 'third'
              },
              {
                uuid: 4,
                title: 'fourth'
              }
            ],
            valueType: 'CHECKLIST'
          },
          {
            title: 'Множественный список',
            key: 'checklist_list',
            isEditColumn: true,
            highlight: true,
            isCompactChecklist: true,
            items: [
              {
                uuid: '0',
                title: 'first'
              },
              {
                uuid: '1',
                title: 'second'
              },
              {
                uuid: '2',
                title: 'third'
              }
            ],
            valueType: 'CHECKLIST_LIST'
          },
          {
            title: 'HTML',
            isEditColumn: true,
            isEditHtml: true,
            highlight: true,
            html: () => {
              return 'Значение'
            }
          },
          {
            title: 'Чекбокс',
            key: 'checkbox',
            isEditColumn: true,
            isDisabled: true,
            valueType: 'BOOLEAN'
          },
          {
            title: 'Радио',
            key: 'radio',
            isEditColumn: true,
            isDisabled: false,
            valueType: 'RADIO',
            items: [
              {
                value: 1,
                title: 'first'
              },
              {
                value: 2,
                title: 'second'
              },
              {
                value: 3,
                title: 'third'
              }
            ]
          },
          {
            title: 'Дата',
            key: 'date',
            isEditColumn: true,
            highlight: true,
            valueType: 'DATE'
          },
          {
            title: 'Числа (error если > 100)',
            key: 'integer',
            isEditColumn: true,
            highlight: value => Number(value) > 100,
            isAlignRight: true,
            valueType: 'INTEGER'
          },
          {
            title: 'Дроби',
            key: 'float',
            isEditColumn: true,
            isAlignRight: true,
            valueType: 'FLOAT'
          }
        ],
        editableItems: [
          {
            text: null,
            money: 100.40,
            checklist: 0,
            checklist_list: null,
            checkbox: true,
            radio: 2,
            date: '2010-01-01',
            integer: 55,
            float: 20.05
          },
          {
            text: 'very long text field with text-overflow example',
            money: 965449.01,
            checklist: null,
            checklist_list: ['0', '2'],
            checkbox: false,
            radio: 3,
            date: '2007-12-24',
            integer: 2019,
            float: 11.9
          },
          {
            text: 'third',
            money: 50.00,
            checklist: 1,
            checklist_list: null,
            checkbox: true,
            radio: 1,
            date: '2020-06-01',
            editDisable: true,
            integer: 1234,
            float: 2000
          },
          {
            text: 'fourth',
            money: 97563.1,
            checklist: null,
            checklist_list: ['1'],
            checkbox: true,
            radio: 2,
            date: '2012-11-11',
            integer: 90,
            float: 10000.055551,
            valueType: 'INTEGER'
          }
        ],
        optionsTableSaveItem: {
          onSubmit: (data, value) => {
            console.log(data, value)
          }
        },
        selected: {},
        selectedUuid: null,
        searchQuery: '',
        participantColumns: [ { 'title': 'Выбор', 'slotName': 'checkbox', 'width': 40, 'fixed': true }, { 'title': 'Группа или контрагент', 'key': 'name', 'fixed': true, 'sortDisable': true }, { 'title': 'Сотрудник', 'key': 'fio' }, { 'title': 'Телефон', 'key': 'phone' }, { 'title': 'Эл. почта', 'key': 'email' } ],
        participantColumns2: [ { 'title': 'Группа или контрагент', 'key': 'name', 'fixed': true, 'sortDisable': true }, { 'title': 'Сотрудник', 'key': 'fio' }, { 'title': 'Телефон', 'key': 'phone' }, { 'title': 'Эл. почта', 'key': 'email' } ],
        participant: [ { 'name': 'ООО «Абсоюлют сервис»', 'inn': '7715962150', 'phone': '+7 (495) 856-5555', 'email': 'info@rom.ru', 'description': 'Возим рефами по Москве', 'address': 'г. Москва, ул. Докукина, д.8, оф. 3', 'uuid': '1', 'fio': 'Андреев Вадим', 'groups': [ 'Авто. Только по ЦАО', 'Минимальные затраты', 'Работали в 2019' ], 'checked': false, 'group': 'Авто. Только по ЦАО' }, { 'name': 'ООО «Абсоюлют сервис»', 'inn': '7715962150', 'phone': '+7 (495) 856-5555', 'email': 'info@rom.ru', 'description': 'Возим рефами по Москве', 'address': 'г. Москва, ул. Докукина, д.8, оф. 3', 'uuid': '1', 'fio': 'Андреев Вадим', 'groups': [ 'Авто. Только по ЦАО', 'Минимальные затраты', 'Работали в 2019' ], 'checked': false, 'group': 'Минимальные затраты' }, { 'name': 'ООО «Абсоюлют сервис»', 'inn': '7715962150', 'phone': '+7 (495) 856-5555', 'email': 'info@rom.ru', 'description': 'Возим рефами по Москве', 'address': 'г. Москва, ул. Докукина, д.8, оф. 3', 'uuid': '1', 'fio': 'Андреев Вадим', 'groups': [ 'Авто. Только по ЦАО', 'Минимальные затраты', 'Работали в 2019' ], 'checked': false, 'group': 'Работали в 2019' }, { 'name': 'ООО «Автосибирь»', 'inn': '7715962659', 'phone': '+7 (499) 258-3321, доб. 24', 'email': 'vasilyok114@yandex.ru', 'description': 'Скорость, надежность, качество', 'address': 'г. Москва, ул. Летчика Бабушкина, д.8, оф. 3', 'uuid': '2', 'fio': 'Куршева Елена', 'groups': [ 'Авто. Только по ЦАО', 'Любимые поставщики Евгинько', 'Минимальные затраты' ], 'checked': false, 'group': 'Авто. Только по ЦАО' }, { 'name': 'ООО «Автосибирь»', 'inn': '7715962659', 'phone': '+7 (499) 258-3321, доб. 24', 'email': 'vasilyok114@yandex.ru', 'description': 'Скорость, надежность, качество', 'address': 'г. Москва, ул. Летчика Бабушкина, д.8, оф. 3', 'uuid': '2', 'fio': 'Куршева Елена', 'groups': [ 'Авто. Только по ЦАО', 'Любимые поставщики Евгинько', 'Минимальные затраты' ], 'checked': false, 'group': 'Минимальные затраты' }, { 'name': 'ООО «Автосибирь»', 'inn': '7715962659', 'phone': '+7 (499) 258-3321, доб. 24', 'email': 'vasilyok114@yandex.ru', 'description': 'Скорость, надежность, качество', 'address': 'г. Москва, ул. Летчика Бабушкина, д.8, оф. 3', 'uuid': '2', 'fio': 'Куршева Елена', 'groups': [ 'Авто. Только по ЦАО', 'Любимые поставщики Евгинько', 'Минимальные затраты' ], 'checked': false, 'group': 'Любимые поставщики Евгинько' }, { 'name': 'ООО «Атмаш»', 'inn': '7715962569', 'phone': '+7 903 555-1782', 'email': 'info@soltrans.ru', 'description': 'Аренда фур, аутсорс водителей', 'address': 'г. Москва, пр. Ленина, д.8, оф. 100', 'uuid': '3', 'fio': 'Жукова Анастасия', 'groups': [ 'Авто. Только по ЦАО', 'Любимые поставщики Евгинько' ], 'checked': false, 'group': 'Авто. Только по ЦАО' }, { 'name': 'ООО «Атмаш»', 'inn': '7715962569', 'phone': '+7 903 555-1782', 'email': 'info@soltrans.ru', 'description': 'Аренда фур, аутсорс водителей', 'address': 'г. Москва, пр. Ленина, д.8, оф. 100', 'uuid': '3', 'fio': 'Жукова Анастасия', 'groups': [ 'Авто. Только по ЦАО', 'Любимые поставщики Евгинько' ], 'checked': false, 'group': 'Любимые поставщики Евгинько' }, { 'name': 'ООО «Байкальская нерпа»', 'inn': '3415962158', 'phone': '+7 (499) 851-1218, доб. 153', 'email': 'info@jupiter-1.ru', 'description': 'Наш приоритет — Южное направление', 'address': 'г. Москва, Новорязанское ш., д.104', 'uuid': '4', 'fio': 'Иванов Владимир', 'groups': [ 'Любимые поставщики Евгинько', 'Минимальные затраты', 'Работали в 2019' ], 'checked': false, 'group': 'Минимальные затраты' }, { 'name': 'ООО «Байкальская нерпа»', 'inn': '3415962158', 'phone': '+7 (499) 851-1218, доб. 153', 'email': 'info@jupiter-1.ru', 'description': 'Наш приоритет — Южное направление', 'address': 'г. Москва, Новорязанское ш., д.104', 'uuid': '4', 'fio': 'Иванов Владимир', 'groups': [ 'Любимые поставщики Евгинько', 'Минимальные затраты', 'Работали в 2019' ], 'checked': false, 'group': 'Работали в 2019' }, { 'name': 'ООО «Байкальская нерпа»', 'inn': '3415962158', 'phone': '+7 (499) 851-1218, доб. 153', 'email': 'info@jupiter-1.ru', 'description': 'Наш приоритет — Южное направление', 'address': 'г. Москва, Новорязанское ш., д.104', 'uuid': '4', 'fio': 'Иванов Владимир', 'groups': [ 'Любимые поставщики Евгинько', 'Минимальные затраты', 'Работали в 2019' ], 'checked': false, 'group': 'Любимые поставщики Евгинько' }, { 'name': 'ООО «Бетонстрой»', 'inn': '0815499989', 'phone': '+7 (8712) 51-1218, доб. 8', 'email': '—', 'description': '—', 'address': 'г. Грозный, ул. Курсантов, д.75, оф. 153', 'uuid': '5', 'fio': 'Кулаков Николай', 'groups': [ 'Любимые поставщики Евгинько' ], 'checked': false, 'group': 'Любимые поставщики Евгинько' } ]
      }
    },
    props: {
      title: {
        default: text('title', 'Заголовок формы')
      },
      functionalPanel: {
        default: boolean('functionalPanel', true)
      },
      showAddButton: {
        default: boolean('showAddButton', false)
      },
      addButtonText: {
        default: text('addButtonText', 'Добавить')
      },
      showEdit: {
        default: boolean('showEdit', true)
      },
      showHiddenLength: {
        default: boolean('showHiddenLength', true)
      },
      isCssSelectable: {
        default: boolean('isCssSelectable', true)
      },
      showRefresh: {
        default: boolean('showRefresh', true)
      },
      isHeader: {
        default: boolean('isHeader', true)
      },
      tableHeight: {
        default: text('tableHeight', '')
      },
      footerDisabled: {
        default: boolean('footerDisabled', false)
      },
      tableLayoutFixed: {
        default: boolean('tableLayoutFixed', false)
      },
      pageLimit: {
        default: text('pageLimit', 50)
      },
      groupBy: {
        default: text('groupBy', 'enabled')
      },
      loading: {
        default: boolean('loading', false)
      },
      withCheckboxes: {
        default: boolean('withCheckboxes', false)
      },
      updateTimeout: {
        default: number('updateTimeout', 10000)
      }
    },
    components: { ObTable, ObFuncPanel, ObCheckbox },
    template: `
      <div style='margin: 50px'>

        <h3>Table</h3>
        <br>
        {{selectedUuid}}
        <br>
        <ObTable
          ref="ob-table"
          :items="items"
          :columns="columns"
          :functionalPanel="functionalPanel"
          :showAddButton="showAddButton"
          :showEdit="showEdit"
          :showRefresh="showRefresh"
          :isCssSelectable="isCssSelectable"
          :filterButtons="filterButtons"
          :addButtons="addButtons"
          :addButtonsOptions="addButtonsOptions"
          :showHiddenLength="showHiddenLength"
          :isHeader="isHeader"
          :tableHeight="tableHeight"
          :addButtonText="addButtonText"
          :footerDisabled="footerDisabled"
          :tableLayoutFixed="tableLayoutFixed"
          :withCheckboxes="withCheckboxes"
          :loading="loading"
          :groupBy="groupBy"
          @select="onSelect"
          @add="onAdd"
          @onRefresh="onAdd('refresh')"
          @onEdit="onAdd('edit')"
          @on-change-filter-button="onChangeFilterButton"
          group-by="enabled"
          :group-title-html="groupTitle"
        >
          <template #extraFuncToPanel>
            <button>BUTTON</button>
          </template>

          <div slot="enabled" slot-scope="{ data }">
            <div>
              <ObCheckbox v-model="data.item.enabled" :disabled="true" />
            </div>
          </div>
        </ObTable>

        <br><br>
        selected: {{ selected }}
        <br><br><br>

        <br><br>
        <span @click="selected = {}; $refs['ob-table'].deselect()">снять выбранное поле</span>
        <br><br>
        <span @click="items = items.concat(items)">добавить строк</span>
        <br><br><br>

        <br><br>
        <span @click="updateItems">Обновить ITEMs</span>
        <br><br><br>

        <div :style="box">
          <h4 :style="h4">Properties</h4>
          <div :style="violet">:items <span style="color: #848484">// *ARRAY, required* <span style="color: #fff"> – list of elements</span></span></div>
          <div :style="violet">:columns <span style="color: #848484">// *ARRAY, required*<span style="color: #fff"> – list of columns</span></span></div>
          <div :style="violet">:selected <span style="color: #848484">// *OBJECT, required*<span style="color: #fff"> – selected object, selection background relies on this prop</span></span></div>
          <div :style="violet">:isHeader <span style="color: #848484">// *[Boolean], not required*<span style="color: #fff"> – Hide header of table</span></span></div>
          <div :style="violet">:checkedRows <span style="color: #848484">// *[Array], not required*<span style="color: #fff"> – Checked rows</span></span></div>
          <div :style="violet">:functionalPanel <span style="color: #848484">// *BOOLEAN, not required*<span style="color: #fff"> – functional panel visibility</span></span></div>
          <div :style="violet">:isFunctionalSort <span style="color: #848484">// *[Boolean], not required*<span style="color: #fff"> – Скрыть/показать кнопку "сортировки" на функ. панели</span></span></div>
          <div :style="violet">:isFunctionalGroup <span style="color: #848484">// *[Boolean], not required*<span style="color: #fff"> – Скрыть/показать кнопку "группировки" на функ. панели</span></span></div>
          <div :style="violet">:showAddButton <span style="color: #848484">// *BOOLEAN, not required*<span style="color: #fff"> – add button visibility</span></span></div>
          <div :style="violet">:addButtonText <span style="color: #848484">// *STRING, not required, 'Добавить' is default*<span style="color: #fff"> – add button text</span></span></div>
          <div :style="violet">:searchQuery <span style="color: #848484">// *STRING, not required*<span style="color: #fff"> – search query from standalone func-panel</span></span></div>
          <div :style="violet">:remote-opt <span style="color: #848484">// *[FUNCTION, OBJECT], not required* <br>
            <span style="color: #fff; padding-left: 20px;">Items игнорируются при переданном параметре</span>
            <br>
            <span style="color: #fff; padding-left: 20px;">FUNCTION - function({ page, size, query, sort })</span>
            <br>
            <span style="color: #fff; padding-left: 20px;">OBJECT - <pre style="padding-left: 20px;">{
  method: function({ page, size, query, sort, ...arguments }), // Ссылка на метод, в аргументах придут стандартные параметры и arguments, если указаны
  arguments: { userArgument: userArgumentValue }, // Будут переданы в метод наряду со стандартными параметрами
  contentKey: contentKeyValue // Ключ для нахождения в объекте массива с данными (по умолчанию content/values)
}</pre>
            </span>
          </span></div>
          <div :style="violet">:remote-trigger <span style="color: #848484">// *[String, Number, Object, Boolean], not required*<span style="color: #fff"> – Если передан параметр, загрузка через remote-opt произойдет только при его наличии. А также при его изменении</span></span></div>
        </div>
        <div :style="box">
          <h4 :style="h4">Information</h4>
          <div :style="violet">columns.html <span style="color: #848484">// *FUNCTION, not required, accepts object as argument (data)*<span style="color: #fff"> – cell html</span></span></div>
          <div :style="violet">columns.key <span style="color: #848484">// *STRING, required*<span style="color: #fff"> – item property to display</span></span></div>
          <div :style="violet">columns.action <span style="color: #848484">// *FUNCTION, not required*<span style="color: #fff"> – action on cell click, prevents from select row</span></span></div>
        </div>
        <div :style="box">
          <h4 :style="h4">Events</h4>
          <div :style="violet">@add <span style="color: #fff"> – when add button is clicked</span></div>
          <div :style="violet">@select <span style="color: #fff"> – returns object from selected row</span></div>
        </div>
        <div :style="box">
          <h4 :style="h4">Slots</h4>
          <div :style="violet">#extraFuncToPanel <span style="color: #fff"> – slot для компонента functionalPanel</span></div>
        </div>

        <br><br><br><br><br><br>

        <ObFuncPanel
          :columns="columns2"
          @search="onSearch"
          @toggle-column-visibility="onToggleColumnVisibility"
        />

        <h3>Table with separate func panel</h3>
        <ObTable
          :items="items"
          :columns="columns2"
          :selected="selected"
          :selectedUuid="selectedUuid"
          :searchQuery="searchQuery"
          :footerDisabled="true"
          :tableLayoutFixed="false"
          @select="onSelect"
        />
        <br>
        <h3>Таблица с фиксацией заголовка и заданной высотой</h3>
        <ObTable
          :items="items"
          :columns="columns2"
          :selectedUuid="selectedUuid"
          :searchQuery="searchQuery"
          :footerDisabled="true"
          :tableLayoutFixed="false"
          :tableHeight=200
          @select="onSelect"
        />
        <br>
        <h3>Таблица с зафиксированным столбцами</h3>
        <ObTable
          :group-by="'group'"
          :columns="participantColumns"
          :items="participant"
          :functionalPanel="true"
          :tableLayoutFixed="false"
          :showAddButton="true"
          :isFunctionalSort="false"
          :is-header="false"
        >
          <div slot="checkbox" slot-scope="{ data }">
            <div @click.stop>
              <ObCheckbox
                v-model="data.item.checked"
              />
            </div>
          </div>
        </ObTable>
        <br>
        <h3>Таблица с зафиксированным столбцами и с фиксацией заголовка</h3>
        <ObTable
          :group-by="'group'"
          :columns="participantColumns"
          :items="participant"
          :functionalPanel="true"
          :tableLayoutFixed="false"
          :showAddButton="true"
          :tableHeight=300
        >
          <div slot="checkbox" slot-scope="{ data }">
            <div @click.stop>
              <ObCheckbox
                v-model="data.item.checked"
              />
            </div>
          </div>
        </ObTable>

        <br>

        <h3>Таблица с закрытыми по-умолчанию группами</h3>

        <ObTable
          :group-by="'group'"
          :columns="participantColumns"
          :items="participant"
          :is-groups-opened="false"
          :tableHeight="300"
        >
          <div slot="checkbox" slot-scope="{ data }">
            <div @click.stop>
              <ObCheckbox v-model="data.item.checked" />
            </div>
          </div>
        </ObTable>

        <br>
        <br>
        <br>

        <h3 style="margin-bottom: 10px">Таблица с редактируемыми ячейками</h3>
        
        <p style="margin-bottom: 10px">
          Последняя строка имеет тип <code>INTEGER</code>,
          из-за чего тип столбца игнорируется.
        </p>

        <ObTable
          :columns="columns3"
          :items="editableItems"
          :is-groups-opened="false"
          :options-save-row="optionsTableSaveItem"
          :is-highlight-disabled-columns="true"
        />

        <br>
        <br>
        <br>

        <h3>Таблица с чекбоксами</h3>

        <ObTable
          :group-by="'group'"
          :columns="participantColumns2"
          :items="participant"
          :functionalPanel="true"
          :tableLayoutFixed="false"
          :showAddButton="true"
          :withCheckboxes="true"
          :checkedRows="['1', '4']"
          :staticGroups="['Авто. Только по ЦАО']"
        />

      </div>`,
    computed: {
      box () {
        return {
          'background': '#000',
          'color': '#fff',
          'padding': '12px',
          'margin-bottom': '20px'
        }
      },
      h4 () {
        return {
          'margin': '0 0 14px;'
        }
      },
      violet () {
        return {
          'margin-bottom': '2px',
          'color': '#f382e3'
        }
      }
    },
    methods: {
      onChangeFilterButton (data) {
      },
      onSelect (data) {
        this.selected = data
      },
      onAdd (item) {
      },
      getDatetime (data) {
        return `
          <div>${data.when.split('T')[0]}</div>
          <div>${data.when.split('T')[1]}</div>
        `
      },

      updateItems () {
        this.items = [
          {
            title: 'six title',
            value: 'this is a value',
            when: '2019-10-10T10:49:53.747195Z'
          }
        ]
        this.columns = [
          {
            title: 'Название новое',
            key: 'title'
          },
          {
            title: 'Значение',
            key: 'value',
            action: data => {}
          },
          {
            slotName: 'enabled',
            title: 'Активен',
            key: 'enabled',
            width: 100
          },
          {
            title: 'Дата и время',
            key: 'when',
            html: data => this.getDatetime(data),
            sort: 'datetime'
          }
        ]
      },
      onSearch (query) {
        this.searchQuery = query
      },
      onToggleColumnVisibility (column) {
        this.columns2.find(col => col.uuid === column.uuid)._isEnabled = column._isEnabled
      },
      groupTitle (value) {
        return value ? 'Активен' : 'Неактивен'
      }
    }
  }),
  {
    info: {
      components: { ObTable },
      summary: `
      \`\`\`html
        <ObTable
          ref="ob-table"
          :items="items"
          :columns="columns"
          :functionalPanel="true"
          :showAddButton="false"
          :showEdit="true"
          :showRefresh="true"
          :isCssSelectable="true"
          :filterButtons="filterButtons"
          :addButtons="addButtons"
          :addButtonsOptions="addButtonsOptions"
          :showHiddenLength="true"
          @select="onSelect"
          @add="onAdd"
          @onRefresh="onAdd('refresh')"
          @onEdit="onAdd('edit')"
          @on-change-filter-button="onChangeFilterButton"
          group-by="enabled"
          :group-title-html="groupTitle"
        >
          <template #extraFuncToPanel>
            <button>BUTTON</button>
          </template>

          <div slot="enabled" slot-scope="{ data }">
            <div>
              <ObCheckbox v-model="data.item.enabled" :disabled="true" />
            </div>
          </div>
        </ObTable>
      \`\`\`
      `,
      source: false
    }
  })
