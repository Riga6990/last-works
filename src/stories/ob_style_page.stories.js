/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'
import { required, minLength, maxLength, numeric, requiredIf, email } from 'vuelidate/lib/validators'
import ObForm from '../obComponents/obForm'
import FormBody from './ObFormStories'
import ObModal from '../obComponents/obModal'
import ObTabs from '../obComponents/obTabs'
import ObTabsPane from '../obComponents/obTabs/ObTabsPane'
import ObSelector from '../obComponents/obSelect/index.vue'

const stories = storiesOf('ObStylePage', module)

const checkItemTitle = (value, vm) => {
  // vm - текущий уровень модели
  return Boolean(value && value.length <= 5)
}

function checkItemsLength (value) {
  // this - инстанс компонента ObForm
  return Boolean(!this.formData || !this.formData.count || +this.formData.count === value.length)
}

const checkItemsCount = (value) => {
  // this - инстанс компонента ObForm
  return Boolean(!value || value <= 2)
}

stories.addDecorator(withKnobs)
stories.add('sample', () => ({
  components: {
    ObForm,
    ObTabsPane,
    ObTabs,
    FormBody,
    ObModal,
    ObSelector
  },
  props: {
    title: {
      default: text('title', 'Заголовок формы')
    },
    isReadOnly: {
      default: boolean('isReadOnly', false)
    },
    isOnlySubmit: {
      default: boolean('isOnlySubmit', false)
    },
    okText: {
      default: text('okText', 'Сохранить')
    },
    cancelText: {
      default: text('cancelText', 'Отмена')
    },
    isSubmitDisabled: {
      default: select('isSubmitDisabled', [ undefined, true, false ], undefined)
    },
    isShowControls: {
      default: boolean('isShowControls', true)
    },
    isShowRequired: {
      default: boolean('isShowRequired', true)
    },
    isOnPressEnterSubmit: {
      default: boolean('isOnPressEnterSubmit', false)
    },
    isActiveElement: {
      default: select('isActiveElement', [ null, true, false ], null)
    },
    btnModal: {
      default: text('btnModal', '')
    }
  },
  data () {
    return {
      dataSource: {},
      formData: {
        name: '',
        count: '',
        detail: {
          title: '',
          items: []
        }
      },
      item1: {
        title: 'Первый таб (c тултипом)',
        isSelected: true,
        tooltip: { content: 'Контент тултипа' },
        content: 'Ребята, не стоит вскрывать эту тему. Вы молодые, шутливые, вам все легко. Это не то. Это не Чикатило и даже не архивы спецслужб. Сюда лучше не лезть. Серьезно, любой из вас будет жалеть. Лучше закройте тему и забудьте, что тут писалось. Я вполне понимаю, что данным сообщением вызову дополнительный интерес, но хочу сразу предостеречь пытливых - стоп. Остальные просто не найдут'
      },
      item2: {
        title: 'Второй таб (среднестатистический)',
        icon: 'alert',
        content: 'Ребята, давайте вскроем эту тему. Вы ведь уже не молодые, дела даются все сложнее и сложнее. Это же мать Тереза, это Библия, сюда можно без опаски влезать. Серьезно, вы не пожалеете. Лучше еще раз перечитайте что тут написано чтобы не забыть. Я вполне понимаю, что данной писаниной вызываю у вас к делу отвращение, но хочу просто сказать ленивым - ВПЕРЕД! Остальные найдут это благодаря вашим стараниям'
      },
      item3: {
        title: 'Задизейбленный таб',
        isDisabled: true,
        content: 'Bernd you really should not open this topic. You are young, playful, everything\'s easy for you This is an another thing. It\'s not Snowden and not even secret services archives. Better not to get into that. Seriously, any of you would deeply regret. Close the topic and forget what was written here. I fully understand that this post incites additional interest, but I far the curious - stop. The rest will just not see it.'
      },
      tabChangesCount: -1,
      dropdownMenu: [
        {
          title: 'Действие 1',
          method: () => alert('Вы совершили действие 1')
        },
        {
          title: 'Действие 2',
          method: () => alert('Вы совершили действие 2')
        }
      ]
    }
  },
  computed: {
    formValidator () {
      return this.$refs.form && this.$refs.form.$v
    },
    formModel () {
      return {
        name: {
          value: '',
          validator: {
            required,
            minLength: {
              method: minLength(5),
              error: 'Слишком короткое имя'
            },
            maxLength: {
              method: maxLength(10),
              error: 'Слишком длинное имя'
            }
          }
        },
        count: {
          value: '',
          validator: {
            number: {
              method: numeric,
              error: 'Нужно ввести число'
            },
            maxLength: {
              method: checkItemsCount,
              error: 'Не больше двух элементов, парень'
            }
          }
        },
        detail: {
          nested_fields: {
            title: {
              value: '',
              validator: {
                required: requiredIf(function (model) {
                  // model - текущий уровень модели
                  // this - инстанс компонента ObForm
                  return this.formData.count && this.formData.count > 1
                })
              }
            },
            items: {
              type: 'array',
              value: [],
              validator: {
                checkItemsLength: {
                  method: checkItemsLength
                },
                $each: {
                  title: {
                    value: '',
                    validator: {
                      required,
                      maxLength: {
                        method: checkItemTitle,
                        error: 'Имя элемента не должно быть больше 5 символов'
                      }
                    }
                  },
                  email: {
                    value: '',
                    validator: {
                      required,
                      email: {
                        method: email,
                        error: 'Некорректный email'
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  template: `
    <div style='margin: 20px'>
      <div class="row">
        <div class="col-xs-12">
          <ObForm
              ref="form"
              :title="title"
              :is-read-only="isReadOnly"
              :dropdown-menu="dropdownMenu"
              :is-only-submit="isOnlySubmit"
              :is-show-controls="isShowControls"
              :ok-text="okText"
              :cancel-text="cancelText"
              :is-show-required="isShowRequired"
              :is-on-press-enter-submit="isOnPressEnterSubmit"
              :form-model="formModel"
              :form-data="formData"
              :is-active-element="isActiveElement"
              :btn-modal="btnModal"
              @change-active="onChangeActive"
              @on-btn-modal="openModalChild"
          >
            <ObTabs
                :items="[{ title: 'Таб 1' }, { title: 'Таб 2' }, { title: 'Таб 3' }]"
                @on-change-tab="e => {
            tabChangesCount++
            tabChangedMsg = e
          }"
            />

            <div style="padding: 20px;">
          <span v-if="tabChangesCount === 0">
            івенту ще не було(((
          </span>
              <span v-else>
            таб змінюється в {{ tabChangesCount }} раз!
            <br>
            поточне значення: {{ tabChangedMsg }}
          </span>
            </div>
            
          </ObForm>
        </div>
      </div>
      <ObModal
          ref="ob-modal-child"
          size="small"
          :title="btnModal"
      >
        <p>{{ btnModal}}</p>
      </ObModal>
    </div>`,
  methods: {
    action: action('clicked'),
    openModalChild () {
      this.$refs['ob-modal-child'].open()
    },
    onChangeActive (val) {
      alert('is-active state: ' + val)
    }
  }
}))
