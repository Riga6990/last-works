/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ObDraggable from '../obComponents/obDraggable/index'

storiesOf('ObDraggable', module)
  .add('sample', () => {
    return {
      components: { ObDraggable },
      data () {
        return {
          date: new Date()
        }
      },
      template: `
        <div style='margin-top: 100px; margin-left: 100px; width: 350px'>
          <ob-draggable
            :format="format"
            :placeholder="placeholder"
            :label="label"
            :blockAll="blockAll"
            :sinceTo="sinceTo"
            :allowHolidays="allowHolidays"
            :allowWeekends="allowWeekends"
            :allowAll="allowAll"
            :lang="lang"
            :is-inline="isInline"
            @blurAddress="blurAddress"
            v-model="date"
          />
        </div>`,
      methods: {
        blurAddress (data) {
          console.log(data)
        }
      }
    }
  })
