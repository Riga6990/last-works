/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, object, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

import ObMultiRangeSelect from '../obComponents/obRangeSelect/obMultiRangeSelect'

storiesOf('ObMultiRangeSelect', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObMultiRangeSelect },
      props: {
        value: {
          type: Array,
          default: object('Value', [{ end: 40, start: 20, color: 'green', startLabel: 'Минимальное время', endLabel: 'Максимальное время' }, { end: 80, start: 60, color: 'green', startLabel: 'Минимальное время', endLabel: 'Максимальное время' }])
        },
        startVal: {
          type: Number,
          default: number('Start Value', 0)
        },

        endVal: {
          type: Number,
          default: number('End value', 100)
        },

        units: {
          type: [Array, String],
          default: object('Units', [
            {
              label: 'кг',
              uuid: 2
            },
            {
              label: 'мин.',
              uuid: 3
            }])
        }
      },

      template: `
        <div class="element_container">
          <ObMultiRangeSelect
            :value="value"
            :startVal="startVal"
            :endVal="endVal"
            :units="units"
          />
        </div>
      `,

      description: {
        ObMultiRangeSelect: {
          props: {
            value: 'Array of objects. Each obejct describes the interval and has such fileds: start, end, color, startLabel, endLabel',
            startVal: 'Startng value',
            endVal: 'Ending value',
            units: 'Measurement units'
          }
        }
      }
    }
  },
  {
    info: {
      summary: `Base multirange slider`
    }
  }
  )
