import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObStepper from '../obComponents/obStepper/index.vue'

const stories = storiesOf('ObStepper', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    label: {
      default: text('label', 'Количество')
    },
    step: {
      default: number('step', 1)
    },
    minValue: {
      default: number('minValue', 1)
    },
    maxValue: {
      default: number('maxValue', 100)
    },
    allowNegatives: {
      default: boolean('Negatives', false)
    },
    disabled: {
      default: boolean('disabled', false)
    },
    readonly: {
      default: boolean('readonly', false)
    },
    required: {
      default: boolean('required', false)
    },
    maxErrorTooltip: {
      default: text('maxErrorTooltip', '')
    },
    minErrorTooltip: {
      default: text('minErrorTooltip', '')
    },
    requiredErrorTooltip: {
      default: text('requiredErrorTooltip', '')
    }
  },
  data () {
    return {
      val: ''
    }
  },
  components: { ObStepper },
  template: `
    <div class="element_container">
      <ObStepper
        :label="label"
        :disabled="disabled"
        :readonly="readonly"
        :required="required"
        :minValue="minValue"
        :maxValue="maxValue"
        :maxErrorTooltip="maxErrorTooltip"
        :minErrorTooltip="minErrorTooltip"
        :requiredErrorTooltip="requiredErrorTooltip"
        :step="step"
        :allowNegatives="allowNegatives"
        v-model.number="val"
      />

      <br>
      value: {{ val }}
    </div>`,
  methods: { action: action('clicked') }
}),
{
  info: {
    components: { ObStepper },
    summary: `
      \`\`\`html
        <ObStepper
          :label="label"
          :disabled="disabled"
          :readonly="readonly"
          :required="required"
          :step="step"
          :allowNegatives="allowNegatives"
          v-model.number="val"
        />
      \`\`\`
      `,
    source: false
  }
})
