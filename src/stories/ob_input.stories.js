/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, select, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObInput from '../obComponents/obInput'

const stories = storiesOf('ObInput', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    label: {
      default: text('label', 'Куда')
    },
    required: {
      default: boolean('required', false)
    },
    labelInfo: {
      default: text('labelInfo', 'Здесь будет какая полезная информация')
    },
    placeholder: {
      default: text('placeholder', 'Hint')
    },
    type: {
      default: select('type', [ 'text', 'number', 'number:strict', 'password', 'money', 'money:0', 'money:2' ], 'text')
    },
    readonly: {
      default: boolean('readonly', false)
    },
    disabled: {
      default: boolean('disabled', false)
    },
    minValue: {
      default: number('minValue', null)
    },
    icon: {
      default: select('icon', [ undefined, 'map', 'police', 'airplane', 'question' ], 'map')
    },
    iconClass: {
      default: text('iconClass', null)
    },
    preIcon: {
      default: select('preIcon', [ '', 'city-sm', 'airplane', 'question' ], 'city-sm')
    },
    preIconInfo: {
      default: text('preIconInfo', 'Опорная точка')
    },
    tooltip: {
      default: text('tooltip', 'Информационное сообщение')
    },
    mask: {
      default: text('mask', '')
    },
    isMasked: {
      default: boolean('isMasked', false)
    },
    isValidationMask: {
      default: boolean('isValidationMask', false)
    },
    isValid: {
      default: select('isValid', [ null, true, false ], null)
    },
    validationMessage: {
      default: text('validationMessage', 'Инвалидное поле')
    },
    subTitle: {
      default: text('subTitle', 'кг')
    },
    subTitleItems: {
      default: boolean('subTitleItems', false)
    },
    subTitleLabel: {
      default: text('subTitleLabel', 'Введите объем')
    },
    remind: {
      default: text('remind', '')
    },
    remindTo: {
      default: text('remindTo', '')
    },
    theme: {
      default: select('theme', [ 'default', 'blue' ], 'default')
    },
    large: {
      default: boolean('large', false)
    },
    mini: {
      default: boolean('mini', false)
    },
    isConvertMoney: {
      default: boolean('isConvertMoney', true)
    },
    link: {
      default: text('link', 'http://localhost:6006/?path=/story/obinput--sample')
    },
    isChange: {
      default: boolean('обрабатывать событие change', false)
    }
  },
  data () {
    return {
      value: '',
      subTitleItemsOptions: [
        { title: 'кг', uuid: 'кг' },
        { title: 'm2', uuid: 'm2' },
        { title: 'm1', uuid: 'm1' }
      ]
    }
  },
  components: { ObInput },
  mounted () {
    this.$refs.input.dirty = true
  },
  template: `
  <div class="element_container">
    <ObInput
      v-model = 'value'
      :label = 'label'
      :required = 'required'
      :type='type'
      :label-info = 'labelInfo'
      :placeholder = 'placeholder'
      :disabled = 'disabled'
      :readonly = 'readonly'
      :min-value="minValue"
      :is-valid = 'isValid'
      :pre-icon = 'preIcon'
      :pre-icon-info = 'preIconInfo'
      :tooltip="tooltip"
      :icon='icon'
      :icon-class='iconClass'
      :validation-message='validationMessage'
      :remind='remind'
      :remind-to='remindTo'
      :mask='mask'
      :is-validation-mask='isValidationMask'
      :is-masked='isMasked'
      :sub-title.sync='subTitle'
      :sub-title-items='subTitleItems ? subTitleItemsOptions : undefined'
      :sub-title-label='subTitleLabel'
      :theme="theme"
      :large="large"
      :mini="mini"
      :is-convert-money="isConvertMoney"
      :link="link"
      ref="input"
      @on-click-icon="onClickIcon"
      @change="onChange"
      >
      
    </ObInput>
    <br>
    <strong>Output:</strong>
    {{ value || ' ' }}
    <br>
    <br>
    <h3>Mask tokens:</h3>
    <br>
<pre>
'9': { pattern: /\\d/ },
'д': { pattern: /\\d/ },
'м': { pattern: /\\d/ },
'г': { pattern: /\\d/ },
'ч': { pattern: /\\d/ },
x: { pattern: /[а-яёa-z0-9]/i },
a: { pattern: /[а-яёa-z]/i },
U: { pattern: /[а-яёa-z]/i, transform: v => v.toLocaleUpperCase() },
u: { pattern: /[а-яёa-z]/i, transform: v => v.toLocaleLowerCase() },
'!': { escape: true }
</pre>
    <br>
    <br>
    <h3>Type</h3>
    <br>
    <p><strong>money</strong> - форматирование числа по разрядам, поддерживает модификаторы. Например,<br><strong>money:0</strong> - число без дробной части</p>
    <br>
    <p><strong>isConvertMoney</strong> (по умолчанию: true) - конвертирует входящее значение в рубли и отдачу обратно в копейки</p>
    <br>
    <p><strong>word</strong> - разрешен ввод только символов русского и английского алфавита</p>
    <br>
  </div>`,
  methods: {
    action: action('clicked'),
    onClickIcon () {
      alert('onClickIcon')
    },
    onChange (val) {
      if (this.isChange) {
        alert(`Сработало событие change - значение: ${val}`)
      }
    }
  }
}),
{
  info: {
    components: { ObInput },
    summary: `
      \`\`\`html
        <ObInput
          v-model = 'value'
          :label = 'label'
          :required = 'required'
          :type='type'
          :label-info = 'labelInfo'
          :placeholder = 'placeholder'
          :disabled = 'disabled'
          :readonly = 'readonly'
          :min-value="minValue"
          :is-valid = 'isValid'
          :pre-icon = 'preIcon'
          :pre-icon-info = 'preIconInfo'
          :tooltip="tooltip"
          :icon='icon'
          :icon-class='iconClass'
          :validation-message='validationMessage'
          :remind='remind'
          :remind-to='remindTo'
          :mask='mask'
          :is-validation-mask='isValidationMask'
          :is-masked='isMasked'
          :sub-title.sync='subTitle'
          :sub-title-items='subTitleItems ? subTitleItemsOptions : undefined'
          :sub-title-label='subTitleLabel'
          :theme="theme"
          :large="large"
          :is-convert-money="isConvertMoney"
          ref="input"
          @on-click-icon="onClickIcon"
          >
      \`\`\`
    `,
    source: false
  }
})
