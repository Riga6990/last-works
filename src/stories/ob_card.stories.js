/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import {
  withKnobs,
  text,
  object,
  array,
  optionsKnob as options
} from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObCard from '../obComponents/obCard'

storiesOf('ObCard', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    components: { ObCard },
    props: {
      type: {
        default: options(
          'type',
          { down: 'down', up: 'up' },
          'down',
          { display: 'inline-radio' }
        )
      },
      title: {
        default: text('title', 'ООО “ФМ Лоджистик Восток”')
      },
      subtitle: {
        default: text('subtitle', 'Москва')
      },
      date: {
        default: text('date', '25.06.2019')
      },
      time: {
        default: text('time', '13:00-15:00')
      },
      locationLabel: {
        default: text('location-label', 'Фактический адрес')
      },
      location: {
        default: text('location', 'г. Нижний Новгород, ул. Николаева, 26')
      },
      locationDocLabel: {
        default: text('location-doc-label', 'Адрес для документации (ТСД)')
      },
      locationDoc: {
        default: text('location-doc', 'г. Нижний Новгород, ул. Лобачевского, 27')
      },
      companies: {
        default: array('companies', [
          {
            name: 'ООО “Логист Сервис-А”',
            count: '2'
          },
          {
            name: 'ООО “Грузоотправитель-2”',
            count: '2'
          },
          {
            name: 'ООО “Грузоотправитель-3”',
            count: '6'
          }
        ])
      },
      error: {
        default: object('error', {
          date: '',
          time: '',
          location: '',
          locationDoc: ''
        })
      }
    },
    template: `
      <div style="margin: 30px">

        <ObCard
          :type="type"
          title="Укажите грузоотправителя"
          :subtitle="subtitle"
        />

        <br />

        <ObCard
          :type="type"
          :title="title"
          :subtitle="subtitle"
          :date="date"
          :time="time"
          :location="location"
          :location-label="locationLabel"
          :location-doc="locationDoc"
          :location-doc-label="locationDocLabel"
          :companies="companies"
          :error="error"
        />
      </div>`,
    methods: {
      clickHandler (e) {
      }
    }
  }),
  {
    info: {
      components: { ObCard },
      summary: `
      \`\`\`html
        <ObCard
          :type="type"
          :title="title"
          :subtitle="subtitle"
          :date="date"
          :time="time"
          :location="location"
          :location-label="locationLabel"
          :location-doc="locationDoc"
          :location-doc-label="locationDocLabel"
          :companies="companies"
          :error="error"
        />
      \`\`\`
      `,
      source: false
    }
  })
