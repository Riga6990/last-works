/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, select, number } from '@storybook/addon-knobs'

import ObTooltip from '../obComponents/obTooltip'
import ObButton from '../obComponents/obButton'
import { withInfo } from 'storybook-addon-vue-info'

const stories = storiesOf('ObTooltip', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    disabled: {
      default: boolean('disabled', false)
    },
    popperClass: {
      default: text('popperClass', '')
    },
    position: {
      default: select('position', [ 'top', 'top-start', 'top-end', 'bottom', 'bottom-start', 'bottom-end', 'left', 'left-start', 'left-end', 'right', 'right-start', 'right-end' ], 'bottom-end')
    },
    type: {
      default: select('type', ['primary', 'error'], 'error')
    },
    content: {
      default: text('content', 'An error message should be clear and not ambiguous.')
    },
    appendToBody: {
      default: boolean('appendToBody', true)
    },
    openDelay: {
      default: number('openDelay', 300)
    },
    closeAfter: {
      default: number('closeAfter', 10)
    },
    hideArrow: {
      default: boolean('hideArrow', false)
    },
    isHovered: {
      default: boolean('isHovered', false)
    }
  },
  components: {
    ObTooltip,
    ObButton
  },
  data () {
    return {
      value: false
    }
  },
  template: `
  <div class="element_container">
    <ObTooltip
      v-model="value"
      :disabled="disabled"
      :popper-class="popperClass"
      :position="position"
      :type="type"
      :content="content"
      :append-to-body="appendToBody"
      :open-delay="openDelay"
      :close-after="closeAfter"
      :hide-arrow="hideArrow"
      :is-hovered="isHovered"
    >
      <i class="icon question" style="font-size: 24px;"/>
    </ObTooltip>
   <br>
   <br>
   <ObButton
    label="ShowTooltip"
    @click="value = !value"
    :mini="true"
   />
  </div>`,
  methods: {
    action: action('clicked')
  }
}),
{
  info: {
    components: { ObTooltip },
    summary: `
      \`\`\`html
        <ObTooltip
          v-model="value"
          :disabled="disabled"
          :popper-class="popperClass"
          :position="position"
          :type="type"
          :content="content"
          :append-to-body="appendToBody"
          :open-delay="openDelay"
          :close-after="closeAfter"
          :hide-arrow="hideArrow"
          :is-hovered="isHovered"
        >
          <i class="icon question" style="font-size: 24px;"/>
        </ObTooltip>
      \`\`\`
    `,
    source: false
  }
})
