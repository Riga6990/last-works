import { storiesOf } from '@storybook/vue'
import {
  withKnobs,
  object
} from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObDictionaryDocsForm from '../obComponents/obDictionaryDocsForm/index.vue'

storiesOf('ObDictionaryDocsForm', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    components: { ObDictionaryDocsForm },
    props: {
      value: {
        type: Object,
        default: object('value', {
          usedForCleintExpeditor: false,
          usedForExpeditorContractor: false,
          usedForCleintContractor: false,

          amountForClient: null,
          amountForGp: null,
          amountForExpeditor: null,
          amountForContractor: null,

          docType: null,
          creator: null,

          docTypes: [
            { uuid: 1, title: 'Тип 1' },
            { uuid: 2, title: 'Тип 2' },
            { uuid: 3, title: 'Тип 3' },
            { uuid: 4, title: 'Тип 4' }
          ],
          creators: [
            { uuid: 1, title: 'Клиент' },
            { uuid: 2, title: 'Экспедитор' },
            { uuid: 3, title: 'Постащик' },
            { uuid: 4, title: 'Администратор' }
          ]
        })
      }
    },
    data () {
      return {
        currentValue: ''
      }
    },
    template: `
    <div>
      <ObDictionaryDocsForm 
          :value="value" 
          @input="e => { currentValue = e }"
      />
      <p>currentValue = {{currentValue}}</p>
    </div>
      `
  }),
  {
    info: {
      components: { ObDictionaryDocsForm },
      summary: `
      \`\`\`html
        <ObDictionaryDocsForm 
            :value="value" 
            @input="e => { currentValue = e }"
        />
      \`\`\`
      `,
      source: false
    }
  })
