/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, select, object } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObDatepicker from '../obComponents/obDatePicker/index'

const defaultSinceTo = [
  {
    from: '2019-11-18',
    to: '2019-11-21'
  }
]

storiesOf('ObDatepicker', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObDatepicker },
      props: {
        format: {
          type: String,
          default: text('format', 'dd MMMM yyyy')
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Date')
        },
        label: {
          type: String,
          default: text('label', 'Date')
        },
        sinceTo: {
          type: Array,
          default: object('sinceTo', defaultSinceTo)
        },
        blockAll: {
          type: Boolean,
          default: boolean('blockAll', false)
        },
        allowHolidays: {
          type: Boolean,
          default: boolean('allowHolidays', false)
        },
        allowWeekends: {
          type: Boolean,
          default: boolean('allowWeekends', false)
        },
        allowAll: {
          type: Boolean,
          default: boolean('allowAll', false)
        },
        lang: {
          default: select('lang', ['ru', 'en', 'zh'], 'ru')
        },
        isInline: {
          default: boolean('isInline', true)
        }
      },
      data () {
        return {
          date: new Date()
        }
      },
      template: `
        <div class="element_container">
          <ObDatepicker
            :format="format"
            :placeholder="placeholder"
            :label="label"
            :blockAll="blockAll"
            :sinceTo="sinceTo"
            :allowHolidays="allowHolidays"
            :allowWeekends="allowWeekends"
            :allowAll="allowAll"
            :lang="lang"
            :is-inline="isInline"
            v-model="date"
          />
        </div>`
    }
  },
  {
    info: {
      components: { ObDatepicker },
      summary: `
      \`\`\`html
       <ObDatepicker
          :format="format"
          :placeholder="placeholder"
          :label="label"
          :blockAll="blockAll"
          :sinceTo="sinceTo"
          :allowHolidays="allowHolidays"
          :allowWeekends="allowWeekends"
          :allowAll="allowAll"
          :lang="lang"
          :is-inline="isInline"
          v-model="date"
        />
      \`\`\`
      `,
      source: false
    }
  })
