import RestClient from './RestClient'

const Client = new RestClient()

export default {
  async getFile ({ uuid, url }) {
    let restUrl = url || 'oboz2-security-file-storage/v1'
    try {
      let res = await Client.get(`${restUrl}/${uuid}`, { responseType: 'blob' })
      return res.data
    } catch (e) {
      throw e
    }
  },
  async postFile ({ uuid = null, file = {}, options = {} } = {}) {
    try {
      const jsonData = {
        filename: file.name
      }

      const formData = new FormData()

      formData.append('jsonData', JSON.stringify(jsonData))
      formData.append('file', file)

      await Client.post(`https://dev.oboz.app/oboz2-common-file-storage/v1/${uuid}`, formData, options)
    } catch (e) {
      throw e
    }
  }
}
