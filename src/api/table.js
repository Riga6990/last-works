import RestClient from './RestClient'

export const formationParameters = (page, size, q, sort, filters) => {
  let parameters = ''
  if (page !== undefined) parameters += '&page=' + page
  if (size !== undefined) parameters += '&size=' + size
  if (sort && sort.length > 0) {
    sort.forEach(i => {
      let _sort = i.desc ? 'desc' : 'asc'
      parameters += '&sort=' + i.selector + ',' + _sort
    })
  }
  if (filters) {
    for (let prop in filters) {
      if (filters[prop] !== undefined && filters[prop] !== null && filters[prop].length > 0) {
        parameters += '&' + prop + '=' + filters[prop]
      }
    }
  }
  if (q !== undefined && q.length > 0) parameters += '&q=' + q
  if (parameters.length > 0) {
    parameters = '?' + parameters
  }
  console.log(parameters)
  return parameters
}
const Client = new RestClient()

export default {
  async saveGet (url, data) {
    const status = await Client.get('/' + url, data)
    return status
  },
  async savePost (url, data) {
    const status = await Client.post('/' + url, data)
    return status
  }
}
