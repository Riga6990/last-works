import Vue from 'vue'
import { formatNumber } from './common'

Vue.filter('formatNumber', function (value, options) {
  return formatNumber(value, options)
})
