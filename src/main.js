import Vue from 'vue'
import App from './App.vue'
import uuid from 'vue-uuid'

import './helpers/VueFilters'
import '@css/oboz-font.scss'

Vue.use(uuid)

new Vue({
  render: h => h(App)
}).$mount('#app')
