/* eslint-disable import/no-extraneous-dependencies */
// import Vue from 'vue'
import { configure } from '@storybook/vue'
import '@css/oboz-font.scss'

const req = require.context('@/stories', true, /.stories.js$/)

function loadStories () {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
