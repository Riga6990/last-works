FROM node:12 AS builder

WORKDIR /app

COPY ./package.json /app
COPY ./package-lock.json /app

RUN npm install

COPY . /app/

RUN cd /app && \
    npm run storybook:build

FROM gitlab-01.mgmt.oboz.app:6000/oboz2/build/nginx-spa:latest
WORKDIR /usr/share/nginx/html/
COPY --from=builder /app/storybook-static/ .
RUN mkdir -p ./src/static/css/fonts/
COPY src/static/css/oboz-font.scss ./src/static/css/oboz-font.scss
COPY ./src/static/icon-fonts/* ./src/static/icon-fonts/